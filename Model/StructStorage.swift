//
//  StructStorage.swift
//  Knektapp
//
//  Created by Gowsika on 05/06/18.
//  Modified by Gobi on 03/09/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

/*Favourites Model*/
struct Favourites {
    var serviceName: String?
    var serviceImage: String?
    var favouritesId: String?
    
    init(serviceName: String, serviceImage: String, favouritesId: String) {
        self.serviceName = serviceName
        self.serviceImage = serviceImage
        self.favouritesId = favouritesId
    }
}
/*Main Category Model*/
struct categorys {
    var categoryImages: String?
    var categoryTitle: String?
    var categoryId: Int?
    init(categoryImages: String,categoryTitle: String,categoryId: Int) {
        self.categoryTitle = categoryTitle
        self.categoryImages = categoryImages
        self.categoryId = categoryId
    }
}
/*Service Company details Model*/
struct serviceCategories {
    var serviceImage: String?
    var serviceId: Int?
    var serviceName: String?
    
    init(serviceImage: String, serviceId: Int, serviceName: String) {
        self.serviceImage = serviceImage
        self.serviceId = serviceId
        self.serviceName = serviceName
    }
}

/*Service sub category Model*/
struct serviceSubCompany {
    var serviceSubImages: String?
    var serviceSubNames: String?
    var serviceSubCatId: Int?
    var serviceSubId: Int?
    init(serviceSubImages: String,serviceSubNames: String,serviceSubCatId: Int,serviceSubId: Int) {
        self.serviceSubImages = serviceSubImages
        self.serviceSubNames = serviceSubNames
        self.serviceSubCatId = serviceSubCatId
        self.serviceSubId = serviceSubId
    }
}

/*Brand branch Details Model*/
struct BranchDetails {
    var branchName: String?
    var branchAddress: String?
    var lat: String?
    var lng: String?
    var isExpanded = false
    init(branchName: String, branchAddress: String, lat: String, lng: String, isExpanded: Bool) {
        self.branchName = branchName
        self.branchAddress = branchAddress
        self.lat = lat
        self.lng = lng
        self.isExpanded = false
    }
}
