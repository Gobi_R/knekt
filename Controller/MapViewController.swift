//
//  MapViewController.swift
//  Knektapp
//
//  Created by Gowsika on 17/05/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    var img = String()
    var companyId = 0
    var companyDetail = [String: Any]()
    var viewTitle = ""
    var isFavour: Bool!
    
    @IBOutlet weak var bankLogoIcon: UIImageView!{
        didSet{
            bankLogoIcon.layer.shadowColor = UIColor.black.cgColor
        }
    }
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var mapViewLocate: MKMapView!
    @IBOutlet weak var branchOutlet: UIButton!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var showLessOutlet: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Utilities.showTitle(title: viewTitle, control: self)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "menuDot"), style: .plain, target: self, action: #selector(menuAciton))
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.clear
    }
   
    //MARK:- Api Call for Company Data
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.startAnimate(withMsg: "Loading...")
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        NetworkAdapter.methodPOST(.getCompanyData(company_id: companyId, type: "get"), successCallback: { (response) in
            if let jsonData = self.serialization(data: response.data) as? [String: Any], !jsonData.isEmpty {
                self.companyDetail = jsonData["data"] as! [String: Any]
                if self.companyDetail["favourite"] as? Int != 0 {
                    self.isFavour = true
                    self.navigationItem.setRightBarButtonItems([UIBarButtonItem(image: UIImage(named: "menuDot"), style: .plain, target: self, action: #selector(self.menuAciton)),UIBarButtonItem(image: UIImage(named: "star_fill"), style: .plain, target: self, action: #selector(self.favouritesAction(sender:)))], animated: true)
                } else {
                    self.isFavour = false
                    self.navigationItem.setRightBarButtonItems([UIBarButtonItem(image: UIImage(named: "menuDot"), style: .plain, target: self, action: #selector(self.menuAciton)),UIBarButtonItem(image: UIImage(named: "favourites"), style: .plain, target: self, action: #selector(self.favouritesAction))], animated: true)
                }
                self.descriptionTextView.text = self.companyDetail["service_description"] as? String ?? ""
                self.descriptionTextView.text.isEmpty ? (self.showLessOutlet.isHidden = true) : (self.showLessOutlet.isHidden = false)
                self.bankLogoIcon.sd_setImage(with: URL(string: "\(BaseURL.baseURL)"+"/\(self.companyDetail["service_image"] as? String ?? "")"))
                self.isLatLngString()
            } else {
                Utilities.showAlertView(message: "No such data found")
            }
        }, error: { (error) in
            print("Map serialError: \(error)")
        }) { (moyaError) in
            print("Map serialMoyaError: \(moyaError)")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Errors " + error.localizedDescription)
    }
    
    @IBAction func branchesAction(_ sender: UIButton) {
        let next = storyboard?.instantiateViewController(withIdentifier: "BrandDetailBrancheViewController") as! BrandDetailBrancheViewController
        next.companyId = companyId
        self.navigationController?.pushViewController(next, animated: true)
    }
    
    @IBAction func contactAction(_ sender: UIButton) {
//        let contact = storyboard?.instantiateViewController(withIdentifier: "BankServiceTableViewController") as!
//        BankServiceTableViewController
//        contact.images.append(img)
//        self.navigationController?.pushViewController(contact, animated: true)
    }
    
    @IBAction func moreLessAction(_ sender: UIButton) {
        if sender.tag == 0{
            UIView.animate(withDuration: 0.3) {
                self.descriptionTextView.isScrollEnabled = true
                let height = self.getRowHeightFromText(strText: self.descriptionTextView.text)
                self.heightConstraint.constant = height + 30
                self.view.layoutIfNeeded()
                self.showLessOutlet.setTitle("Less", for: .normal)
                sender.tag = 1
            }
        }
        else{
            UIView.animate(withDuration: 0.3) {
                self.descriptionTextView.isScrollEnabled = false
                self.heightConstraint.constant = 116
                self.view.layoutIfNeeded()
                self.showLessOutlet.setTitle("More", for: .normal)
                sender.tag = 0
            }
        }
    }
    
    /*Getting height of textview to expand and collapse*/
    func getRowHeightFromText(strText : String!) -> CGFloat{
        let textView : UITextView! = UITextView(frame: CGRect(x: self.descriptionTextView.frame.origin.x,y: 0,
                                                              width: self.descriptionTextView.frame.size.width,
                                                              height: 0))
        textView.text = strText
        textView.font = UIFont(name: "Fira Sans", size:  16.0)
        textView.sizeToFit()
        var txtframe : CGRect! = CGRect()
        txtframe = textView.frame
        var size : CGSize! = CGSize()
        size = txtframe.size
        size.height = 50 + txtframe.size.height
        return size.height
    }
    
    
    func serviceCompanyLocation(lat: Double, lng: Double,title: String) {
        let userLocate = CLLocation(latitude: lat, longitude: lng)
        let myLocation = userLocate as CLLocation
        let pin = MKPointAnnotation()
        pin.title = title
        pin.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        self.mapViewLocate.setRegion(MKCoordinateRegionMake(myLocation.coordinate, MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)), animated: true)
        self.mapViewLocate.addAnnotation(pin)
    }
    
    @objc func menuAciton() {
       let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Home", style: .default, handler: { (action) in
             self.navigationController?.popToRootViewController(animated: true)
        }))
        alert.addAction(UIAlertAction(title: "My Account", style: .default, handler: { (action) in
            let acVc = self.storyboard?.instantiateViewController(withIdentifier: "AccountViewController") as! AccountViewController
            self.navigationController?.pushViewController(acVc, animated: true)
        }))
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { (action) in
            
        }))
        alert.addAction(UIAlertAction(title: "Logout", style: .default, handler: { (action) in
            self.navigationController?.popToRootViewController(animated: true)
            Constant.defaults.set(false, forKey: "Logged")
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.navigationController?.present(alert, animated: true, completion: nil)
    }
    @objc func favouritesAction(sender: UIBarButtonItem) {
        isFavour = !isFavour
        self.navigationItem.rightBarButtonItems?.removeAll()
        NetworkAdapter.methodPOST(.addFavourites(company_id: "\(companyId)", type: "add", user_id: "\(Constant.defaults.value(forKey: "userid") as? Int ?? 0)" , value: companyDetail["service_name"] as? String ?? ""), successCallback: { (Moyaresponse) in
            if let jsonData = self.serialization(data: Moyaresponse.data) as? [String: Any] {
                Utilities.showAlertView(message: jsonData["msg"] as? String ?? "")
            }
        }, error: { (moyaError) in
            print(moyaError)
        }) { (moyaFail) in
            print(moyaFail)
        }
        isFavour ? self.navigationItem.setRightBarButtonItems([UIBarButtonItem(image: UIImage(named: "menuDot"), style: .plain, target: self, action: #selector(self.menuAciton)),UIBarButtonItem(image: UIImage(named: "star_fill"), style: .plain, target: self, action: #selector(self.favouritesAction(sender:)))], animated: true) : self.navigationItem.setRightBarButtonItems([UIBarButtonItem(image: UIImage(named: "menuDot"), style: .plain, target: self, action: #selector(self.menuAciton)),UIBarButtonItem(image: UIImage(named: "favourites"), style: .plain, target: self, action: #selector(self.favouritesAction(sender:)))], animated: true)
    }
    /*Checking Fetched lat,lng is String*/
    func isLatLngString() {
        if let latString = self.companyDetail["latitude"] as? String, let lngString = self.companyDetail["longitude"] as? String {
            let lat1 = Double(latString)
            let long1 = Double(lngString)
            Utilities.currentLocationFetch(lat: lat1!, lng: long1!, title: self.companyDetail["notification"] as? String ?? "empty", map: self.mapViewLocate)
        } else {
            let lat1 = Double(self.companyDetail["latitude"] as! Int)
            let long1 = Double(self.companyDetail["longitude"] as! Int)
             Utilities.currentLocationFetch(lat: lat1, lng: long1, title: self.companyDetail["notification"] as? String ?? "empty", map: self.mapViewLocate)
        }
    }
}
