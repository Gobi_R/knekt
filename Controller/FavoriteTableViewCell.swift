//
//  FavoriteTableViewCell.swift
//  Knektapp
//
//  Created by Gowsika on 01/06/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class FavoriteTableViewCell: UITableViewCell {

    @IBOutlet weak var favourListLabel: UILabel!
    @IBOutlet weak var imagesFavourList: UIImageView!
    @IBOutlet weak var deleteOutelt: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
}

}
