//
//  ProfileViewController.swift
//  Knektapp
//
//  Created by Gowsika on 10/05/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate, sendDate{
 
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneNumberTF: UITextField!
    @IBOutlet weak var languageTF: UITextField!
    @IBOutlet weak var profileEditPic: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var monthLabelOutlet: UILabel!
    @IBOutlet weak var dobBackView: UIView!
    @IBOutlet weak var tickMaleOutlet: UIImageView!
    @IBOutlet weak var tickFemaleOutlet: UIImageView!
    @IBOutlet weak var datePickerBackview: UIView!
    @IBOutlet weak var dateDoneOutlet: UIButton!
    @IBOutlet weak var datepickerBackViewOutlet: UIView!
    @IBOutlet weak var getStartedOutlet: UIButton!
    @IBOutlet weak var maleButtonOutlet: UIButton!
    @IBOutlet weak var femaleButtonOutlet: UIButton!
    
   //var delegates: Values?
    let picker = UIImagePickerController()
    var dob = ""
    var code: String!
    var mobileNumber: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tap)
        self.profileEditPic.layer.cornerRadius = self.profileEditPic.frame.size.width / 2
        self.profileEditPic.layer.borderColor = UIColor.black.cgColor
        self.profileEditPic.clipsToBounds = true
        picker.delegate = self
        yearLabel.leftRoundedButton()
        monthLabelOutlet.layer.addBorder(edge: .left, color: .black, thickness: 0.5)
        yearLabel.layer.addBorder(edge: .left, color: .black, thickness: 0.5)
        firstNameTF.setPaddingwithCornerRadious(imgName: "user_icon")
        lastNameTF.setPaddingwithCornerRadious(imgName: "user_icon")
        emailTextField.setPaddingwithCornerRadious(imgName: "mail_icon")
        phoneNumberTF.setPaddingwithCornerRadious(imgName: "mobile_icon")
        languageTF.setPaddingwithCornerRadious(imgName: "language_icon")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        Constant.isProfileEditing ? getStartedOutlet.setTitle("Update", for: .normal) : getStartedOutlet.setTitle("Get Started", for: .normal)
        if let phone = Constant.defaults.value(forKey: "mobileno") as? String {
            phoneNumberTF.text! = phone
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
    @objc func saveAction() {
        let alertShown = UIAlertController(title: "", message: "Saved Successfully!", preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertShown.addAction(alertAction)
        present(alertShown, animated: true, completion: nil)
    }
    
    func noCamera() {
        let alertVC = UIAlertController(title: "No Camera", message: "Sorry, Gallery is not Accessible.", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertVC.addAction(okAction)
        present(alertVC, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        profileEditPic.contentMode = .scaleToFill
        profileEditPic.image = chosenImage
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: Action Methods
    @IBAction func getStartedAction(_ sender: UIButton) {
        
        self.startAnimate(withMsg: "updating...")
        NetworkAdapter.methodPOST(.profileUserDataUpdate(type: "update", userid: "\(Constant.defaults.value(forKey: "userid") as! Int)", name: firstNameTF.text!, lastname: lastNameTF.text!, email: emailTextField.text!, password: Constant.defaults.value(forKey: "password") as! String , phone: phoneNumberTF.text!, language: "en", dob: dob, gender: gender()), successCallback: { (response) in
            if let result = self.serialization(data: response.data) as? [String: Any], !result.isEmpty {
                print(result)
                self.stopAnimate()
                DispatchQueue.main.async {
                    let past = self.storyboard?.instantiateViewController(withIdentifier: "TabListViewController") as? TabListViewController
                    self.navigationController?.pushViewController(past!, animated: true)
                }
            }
        }, error: { (error) in
            self.stopAnimate()
            print("Response error:\(error)")
        }) { (moya) in
            self.stopAnimate()
            print("Moya error:\(moya)")
        }
    }
    
    @IBAction func editProfileAction(_ sender: UIButton) {
        if UIImagePickerController.availableMediaTypes(for: .photoLibrary) != nil{
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            present(picker, animated: true, completion: nil)
        }
        else{
            noCamera()
        }
    }
    
    @IBAction func languageAction(_ sender: UIButton) {
        let alertSelect = UIAlertController(title: "Choose Your Language", message: nil, preferredStyle: .actionSheet)
        alertSelect.addAction(UIAlertAction(title: "English", style: .default, handler: {(action) -> Void in
            self.languageTF.text =  "English"
        }))
        alertSelect.addAction(UIAlertAction(title: "Arabic", style: .default, handler: {(action) -> Void in
            self.languageTF.text = "Arabic"
        }))
        alertSelect.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {(action: UIAlertAction)
            -> Void in
            alertSelect.dismiss(animated: true, completion: nil)
        }))
        present(alertSelect, animated: true, completion: nil)
    }
    
    @IBAction func maleButtonAction(_ sender: UIButton) {
        if sender.tag == 1 {
            setStatus(status: false)
        } else {
            setStatus(status: true)
        }
    }
    
    func setStatus(status: Bool) {
        status ? (femaleButtonOutlet.backgroundColor = Constant.textColor) : (maleButtonOutlet.backgroundColor = Constant.textColor)
        status ? (maleButtonOutlet.backgroundColor = UIColor.white.withAlphaComponent(0.6)) : (femaleButtonOutlet.backgroundColor = UIColor.white.withAlphaComponent(0.6))
//        tickMaleOutlet.isHidden = status
//        tickFemaleOutlet.isHidden = !status
    }
    
    //MARK:- Datepicker Delegates
    @IBAction func datePickerAction(_ sender: UIDatePicker) {
        let components = Calendar.current.dateComponents([.month, .day, .year], from: sender.date)
        if let day = components.day, let month = components.month, let year = components.year {
            print(day,month,year)
            dob = "\(year)-\(month)-\(day)"
            let wmonth = DateFormatter().monthSymbols[month - 1]
            dateLabel.text! = "\(day)"
            monthLabelOutlet.text! = "\(wmonth)"
            yearLabel.text! = "\(year)"
        }
    }
    
    //MARK: Visual Datepicker
    @IBAction func showDatePicker(_ sender: UIButton) {
 //       datePickerBackview.isHidden = false
//        datepickerBackViewOutlet.isHidden = false
//        dateDoneOutlet.isHidden = false
        let vc = storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as! DatePickerViewController
        vc.delegate = self
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func doneAction(_ sender: UIButton) {
 //       datePickerBackview.isHidden = true
//        datepickerBackViewOutlet.isHidden = true
//        dateDoneOutlet.isHidden = true
//        let vc = storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as! DatePickerViewController
//
//        self.navigationController?.show(vc, sender: self) //present(vc, animated: true, completion: nil)
    }
    
    func gender() -> Int {
        return tickMaleOutlet.isHidden ? 1 : 0
    }
    func date(day: Int, month: String, year: Int) {
        self.dateLabel.text = "\(day)"
        monthLabelOutlet.text! = month
        yearLabel.text = "\(year)"
    }
    
    
}

protocol Values {
    func getCode(code: String)
}


