//
//  ServiceSubCompanyViewController.swift
//  Knektapp
//
//  Created by Gobi R. on 21/09/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class ServiceSubCompanyViewController: UIViewController,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var subCompanyCollectionView: UICollectionView!
    var categoryId = Int()
    var subId = Int()
    var subCompanyImages = [serviceSubCompany]()
    var filteredCompanyDetails = [serviceSubCompany]()
    var viewTitle = ""
    var isSearching = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Utilities.showTitle(title: viewTitle, control: self)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "search"), style: .plain, target: self, action: #selector(searchAction))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.startAnimate(withMsg: "Loading...")
        subCompanyImages.removeAll()
        NetworkAdapter.methodPOST(.getSubCategory(category_id: categoryId, search_key: "", user_id: Constant.defaults.value(forKey: "userid") as! Int, type: "subcategory", subcategory_id: "\(subId)"), successCallback: { (response) in
            if let jsonData = self.serialization(data: response.data) as? [String: Any],  let data = jsonData["data"] as? [[String: Any]], !data.isEmpty {
                for subDetails in data {
                    self.subCompanyImages.append(serviceSubCompany(serviceSubImages: subDetails["service_image"] as? String ?? "", serviceSubNames: subDetails["service_name"] as? String ?? "", serviceSubCatId: subDetails["sub_cat_id"] as? Int ?? 0, serviceSubId: subDetails["id"] as? Int ?? 0))
                }
                self.subCompanyCollectionView.reloadData()
                self.stopAnimate()
            }
        }, error: { (error) in
            print("Get subcategory Error: \(error)")
        }) { (moyaError) in
            print("subcategory MoyaError: \(moyaError)")
        }
    }
    
    /*Collectionview Flow layout*/
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/3.1, height: collectionView.frame.height/5)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return isSearching ? filteredCompanyDetails.count : subCompanyImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "subServiceCell", for: indexPath) as! SubServiceCollectionCell
        isSearching ? cell.subCompanyImage.sd_setImage(with: URL(string: "\(BaseURL.baseURL)"+"/\((filteredCompanyDetails[indexPath.row].serviceSubImages!))")) : cell.subCompanyImage.sd_setImage(with: URL(string: "\(BaseURL.baseURL)"+"/\((subCompanyImages[indexPath.row].serviceSubImages!))"))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let mapVc = storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        if let id = subCompanyImages[indexPath.row].serviceSubId {
            mapVc.companyId = id
        } else {
            print("company Details Missing")
        }
        self.navigationController?.pushViewController(mapVc, animated: true)
    }
    
    /*Search bar initialize*/
    @objc func searchAction() {
        Utilities.initSearchBar(control: self)
    }
}

extension ServiceSubCompanyViewController: UISearchBarDelegate {
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        isSearching = true
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard !searchText.isEmpty else {
            subCompanyCollectionView.reloadData()
            return
        }
        filteredCompanyDetails = subCompanyImages.filter({ (item) -> Bool in
            if let title = item.serviceSubNames {
                return title.lowercased().contains(searchText.lowercased())
            }
            else {
                return false
            }
        })
        subCompanyCollectionView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
        searchBar.resignFirstResponder()
        subCompanyCollectionView.reloadData()
        Utilities.hideSearchBar(title: viewTitle, control: self, searchController: searchBar)
        filteredCompanyDetails.removeAll()
    }
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        isSearching = false
        searchBar.resignFirstResponder()
        subCompanyCollectionView.reloadData()
        Utilities.hideSearchBar(title: viewTitle, control: self, searchController: searchBar)
        filteredCompanyDetails.removeAll()
        return true
    }
}


//MARK:- SubCompany CollectionCell Class
class SubServiceCollectionCell: UICollectionViewCell {
    @IBOutlet weak var subCompanyImage: UIImageView!
    
}
