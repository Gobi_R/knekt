//
//  CellCodeTableViewCell.swift
//  Knektapp
//
//  Created by Gowsika on 10/05/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import SDWebImage

class CellCodeTableViewCell: UITableViewCell {

    @IBOutlet weak var countryNameLabel: UILabel!
    @IBOutlet weak var countryCodeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }

}
