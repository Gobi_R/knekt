//
//  LoginStatus.swift
//  Knektapp
//
//  Created by Gobi R. on 12/11/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import Foundation

class LoginStatus {
    static func updateStatus() {
        let status = Constant.defaults.bool(forKey: "Logged")
        var rootVC : UIViewController?
        if(status == true){
            rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabListViewController") as! TabListViewController
        }else{
            rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as! ViewController
        }
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let navigationController = UINavigationController.init(rootViewController: rootVC!)
        appDelegate.window?.rootViewController = navigationController
    }
}
