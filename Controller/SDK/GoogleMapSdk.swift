//
//  GoogleMapSdk.swift
//  Knektapp
//
//  Created by Gowsika on 11/05/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import GoogleMaps
class GoogleMapSdk: NSObject {
    public class func googleMap(viewController: UIViewController){
        let camera = GMSCameraPosition.camera(withLatitude: 9.918395, longitude: 78.148736, zoom: 12.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        if(viewController.isKind(of: ViewController.self)){
            let store = viewController as! ViewController
            store.view = mapView
        }
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: 9.918395, longitude: 78.148736)
        marker.title = "Madurai"
        marker.snippet = "Anna Nagar,Madurai North"
        marker.map = mapView
        
    }
    }

