//
//  GoogleSdk.swift
//  Knektapp
//
//  Created by Gowsika on 10/05/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import GoogleSignIn
class GoogleSdk: NSObject {
    public class func GooglePlus(){
        var error:  NSError?
        if error != nil{
            print(error ?? "google error")
            return
        }
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().clientID = "613156707176-m9bilt0t0bmi5app8iiej22qf9m9bgqj.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.login")
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.me")
        
    }
}
