//
//  TwitterSdk.swift
//  Knektapp
//
//  Created by Gowsika on 10/05/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import TwitterKit
class TwitterSdk: NSObject {
    public class func TwitterLogin() {
        TWTRTwitter.sharedInstance().logIn(completion: { (session, error) in
            if (session != nil) {
                print("signed in as \(String(describing: session?.userName))");
            }
            else {
                print("error: \(String(describing: error?.localizedDescription))");
            }
        })
    }
}
