//
//  ServicesViewController.swift
//  Knektapp
//
//  Created by Gowsika on 28/05/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class ServicesViewController: UIViewController, UISearchBarDelegate {

    @IBOutlet weak var serviceTableView: UITableView!
    
    var searchActive: Bool = false
    let searchController = UISearchController(searchResultsController: nil)
    var filtered = [serviceCategories]()
    var serviceCategory = [serviceCategories]()
    var categoryId = 0
    var viewTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        extendedLayoutIncludesOpaqueBars = true
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "search"), style: .plain, target: self, action: #selector(searchAction))
        Utilities.showTitle(title: viewTitle, control: self)
    }
    
    @objc func searchAction() {
        Utilities.initSearchBar(control: self)
    }
  
    //MARK:- Api Call Geting Subcategory
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        serviceCategory.removeAll()
        self.startAnimate(withMsg: "Loading...")
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        NetworkAdapter.methodPOST(.getSubCategory(category_id: categoryId, search_key: "", user_id: Constant.defaults.value(forKey: "userid") as! Int, type: "", subcategory_id: ""), successCallback: { (response) in
            if let jsonData = self.serialization(data: response.data) as? [String: Any], !jsonData.isEmpty, let data = jsonData["data"] as? [[String: Any]],!data.isEmpty  {
                for values in data {
                    self.serviceCategory.append(serviceCategories(serviceImage: values["category_image"] as! String, serviceId: values["id"] as! Int, serviceName: values["category_name"] as! String))
                    self.stopAnimate()
                    self.serviceTableView.reloadData()
                }
            } else {
            Utilities.showAlertView(message: "No Such Data")
            }
        }, error: { (error) in
            print("servicepage serialize Error: \(error)")
        }) { (moyaError) in
            print("servicepage moyaError: \(moyaError)")
        }
    }
    //MARK:- Search Delegates
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        Utilities.hideSearchBar(title: viewTitle, control: self, searchController: searchBar)
        serviceTableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let searchString = searchBar.text else {
            return
        }
        filtered = serviceCategory.filter({ (item) -> Bool in
            if let title = item.serviceName {
                return title.lowercased().contains(searchString.lowercased())
            }
            else {
                return false
            }
        })
        serviceTableView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
        serviceTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        serviceTableView.reloadData()
    }
    
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        if !searchActive {
            searchActive = true
            serviceTableView.reloadData()
        }
        searchController.searchBar.resignFirstResponder()
    }
}
 //MARK:- TableView Datasource methods
extension ServicesViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchActive ? filtered.count : serviceCategory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: serviceViewTableCell = tableView.dequeueReusableCell(withIdentifier: "serviceCell", for: indexPath) as! serviceViewTableCell
        cell.textLabel?.textColor = Constant.textColor
        searchActive ? (cell.textLabel?.text = filtered[indexPath.row].serviceName ?? "") : (cell.textLabel?.text = serviceCategory[indexPath.row].serviceName ?? "")
        return cell
    }
    
    //MARK: TableView delegate methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let subcompanyVc = storyboard?.instantiateViewController(withIdentifier: "ServiceSubCompanyViewController") as! ServiceSubCompanyViewController
        subcompanyVc.categoryId = self.categoryId
        subcompanyVc.viewTitle = serviceCategory[indexPath.row].serviceName ?? ""
        subcompanyVc.subId = serviceCategory[indexPath.row].serviceId!
        self.navigationController?.pushViewController(subcompanyVc, animated: true)
    }
}

