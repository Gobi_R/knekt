//
//  SecureViewController.swift
//  Knektapp
//
//  Created by Gowsika on 10/05/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import Foundation

class SecureViewController: UIViewController,UITextFieldDelegate {
    
    let numberTextField = UITextField()
    var otpArray = [String]()
    var otpStr = String()
    var MobileNoStr = String()
    var cc = String()
    
    @IBOutlet weak var textField1: UITextField!
    @IBOutlet weak var textField2: UITextField!
    @IBOutlet weak var textField3: UITextField!
    @IBOutlet weak var textField4: UITextField!
    //MARK: Load Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        print(MobileNoStr)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        textField1.textAlignment = .center
        textField1.layer.cornerRadius = 8
        textField1.layer.borderWidth = 0.5
        textField1.layer.masksToBounds = true
        textField2.textAlignment = .center
        textField2.layer.cornerRadius = 8
        textField2.layer.borderWidth = 0.5
        textField2.layer.masksToBounds = true
        textField3.textAlignment = .center
        textField3.layer.cornerRadius = 8
        textField3.layer.borderWidth = 0.5
        textField3.layer.masksToBounds = true
        textField4.textAlignment = .center
        textField4.layer.borderWidth = 0.5
        textField4.layer.cornerRadius = 8
        textField4.layer.masksToBounds = true
//        textField1.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
//        textField2.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
//        textField3.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
//        textField4.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        let tapping: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(disappearKeyboard))
        view.addGestureRecognizer(tapping)
        }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        textField1.becomeFirstResponder()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
   
    @objc func disappearKeyboard(){
        view.endEditing(true)
    
    }
    //MARK: Action Methods
    @IBAction func submitAction(_ sender: UIButton) {
        let value = textField1.text!+textField2.text!+textField3.text!+textField4.text!
        otpStr = otpArray.joined(separator: "")
        if (textField1.text?.isEmpty)! || (textField2.text?.isEmpty)! || (textField3.text?.isEmpty)! || (textField4.text?.isEmpty)! {
            Utilities.showAlertView(message: "Please enter Otp")
        }
//        else if (value == otpStr) {
//            print(value)
//            print(otpStr)
            self.startAnimate(withMsg: "Verifying...")
        WebServiceHandler.otpVerify(viewController: self, mobile: MobileNoStr, otp: value, cc: cc)
 //       }
    }
    @IBAction func reSendAction(_ sender: UIButton) {
        WebServiceHandler.resendOtp(viewController: self, mobile: MobileNoStr, cc: cc)
        }
    
    //MARK: Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if ((textField.text?.count)! < 1 ) && (string.count > 0) {
            if textField == textField1 {
                textField2.becomeFirstResponder()
            }
            
            if textField == textField2 {
                textField3.becomeFirstResponder()
            }
            
            if textField == textField3 {
                textField4.becomeFirstResponder()
            }
            
            if textField == textField4 {
                textField4.resignFirstResponder()
            }
            
            textField.text = string
            return false
        } else if ((textField.text?.count)! >= 1) && (string.count == 0) {
            if textField == textField2 {
                textField1.becomeFirstResponder()
            }
            if textField == textField3 {
                textField2.becomeFirstResponder()
            }
            if textField == textField4 {
                textField3.becomeFirstResponder()
            }
            if textField == textField1 {
                textField1.resignFirstResponder()
            }
            
            textField.text = ""
            return false
        } else if (textField.text?.count)! >= 1 {
            textField.text = string
            return false
        }
        
        return true
    }
    
//    @objc func textFieldDidChange(textField: UITextField) {
//        print(textField.tag)
//        if otpArray[textField.tag].va == nil {
//            otpArray.insert(textField.text!, at: textField.tag)
//        } else {
//            otpArray.remove(at: textField.tag)
//            otpArray.insert(textField.text!, at: textField.tag)
//        }
//
//        let text = textField.text
//        if text?.utf16.count == 1 {
//            switch textField {
//            case textField1:
//                textField2.becomeFirstResponder()
//            case textField2:
//                textField3.becomeFirstResponder()
//            case textField3:
//                textField4.becomeFirstResponder()
//            case textField4:
//                textField4.resignFirstResponder()
//            default:
//                break
//            }
//        }
//    }
    func didReceiveVerifyResponse(result: [String: Any]) {
       
        if result["type"] as? String == "error" {
             Utilities.showAlertView(message: (result["message"] as? String)!)
            self.stopAnimate()
        } else {
            DispatchQueue.main.async {
                self.stopAnimate()
                let profileVc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                profileVc.code = self.cc
                profileVc.mobileNumber = self.MobileNoStr
                self.navigationController?.pushViewController(profileVc, animated: true)
            }
        }
        }
    func didReceiveResendResponse(_ result: AnyObject){
        
    }
    
}
