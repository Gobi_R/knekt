//
//  WebServiceHandler.swift
//  Knektapp
//
//  Created by Gowsika on 10/05/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import Alamofire

class WebServiceHandler: NSObject {
    
    class func sendOtp(viewController: UIViewController, mobile: String, cc: String){
        let urlString = "http://control.msg91.com/api/sendotp.php"
        let url = URL(string: urlString)
        let session = URLSession.shared
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
//        231535AfCJDaQidr5b718ccf
//        229039AgW9a4kor5b5ffd50
        let postString = "authkey=231535AfCJDaQidr5b718ccf& message=\("Your verification code for Knekt+ is ##OTP##")&sender=\("OTPSMS")&mobile=\(cc)\(mobile)&otp_expiry=2"
        request.httpBody = postString.data(using: .utf8)
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard  error == nil
                else{
                    return
            }
            guard let data = data  else {
                return
            }
            do{
                print(data)
                let backToString = String(data: data, encoding: String.Encoding.utf8) as String?
                print(backToString!)
                if let data1 = backToString!.data(using: .utf8){
                    if let json = try JSONSerialization.jsonObject(with: data1, options: .mutableContainers) as? [String: Any]{
                        print(json)
                        if(viewController.isKind(of: ViewController.self)) {
                            let store = viewController as! ViewController
                       //     store.didReceiveCategoryResponse(json as AnyObject)
                            print(json)
                        }
                    }}
            }
            catch let error{
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
    
    class func otpVerify(viewController: UIViewController, mobile:String,otp: String, cc: String){
        let urlString = "https://control.msg91.com/api/verifyRequestOTP.php"
        let url = URL(string: urlString)!
        let session = URLSession.shared
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postString = "authkey=231535AfCJDaQidr5b718ccf&mobile=\(cc)\(mobile)&message=\("Verified")&otp=\(otp)&otp_expiry=5"
        request.httpBody = postString.data(using: .utf8)
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data,
            response, error in
            guard error == nil else{
                return
            }
            guard let data = data else {
                return
            }
            do{
                print(data)
                let backToString = String(data: data, encoding: String.Encoding.utf8) as String?
                print(backToString!)
                if let data1 = backToString!.data(using: .utf8) {
                    if let json = try JSONSerialization.jsonObject(with: data1, options: .mutableContainers) as? [String: Any] {
                        print(json)
                        if(viewController.isKind(of: SecureViewController.self)) {
                            let used = viewController as! SecureViewController
                            used.didReceiveVerifyResponse(result: json)
                        }
                    }
                }
                
            }
            catch let error{
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
    class func resendOtp(viewController:UIViewController,mobile:String, cc: String){
        let urlString = "http://control.msg91.com/api/retryotp.php"
        let url = URL(string: urlString)!
        let session = URLSession.shared
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postString = "authkey=231535AfCJDaQidr5b718ccf&mobile=\(cc)\(mobile)"
        request.httpBody = postString.data(using: .utf8)
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard error == nil else {
                return
            }
            guard let data = data else {
                return
            }
            do {
                print(data)
                let backToString = String(data: data, encoding: String.Encoding.utf8) as String?
                print(backToString!)
                if let data1 = backToString!.data(using: .utf8) {
                    if let json = try JSONSerialization.jsonObject(with: data1, options: .mutableContainers) as? [String: Any] {
                        print(json)
                        if(viewController.isKind(of: ViewController.self)){
                            let store = viewController as! SecureViewController
                            store.didReceiveResendResponse(json as AnyObject)
                        }}}
            }
            catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
}
