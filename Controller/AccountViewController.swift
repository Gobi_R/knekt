//
//  AccountViewController.swift
//  Knektapp
//
//  Created by Gowsika on 31/05/18.
//  Copyright © 2018 com.dci. All rights reserved.
//
import UIKit
import Photos

class AccountViewController: BaseViewController, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, Values  {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var ProfileNameLabel: UILabel!
//    @IBOutlet weak var myAccOutlet: UIButton!
//    @IBOutlet weak var favouriteOutlet: UIButton!
//    @IBOutlet weak var recentOutlet: UIButton!
//    @IBOutlet weak var accountLabel: UILabel!
//    @IBOutlet weak var favouriteLabel: UILabel!
//    @IBOutlet weak var recentLabel: UILabel!
    @IBOutlet weak var accountDetailTable: UITableView!
    
    var detailLbl = ["Mobile", "Email", "Password", "Language" ]
    var txt = ["+965 0156423", "xyz23@gmail.com", "*********", "English"]
    var log = ["Home", "Logout"]
    let picker = UIImagePickerController()
    var getValue: String?
    let slide = BaseViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        favouriteLabel.isHidden = true
//        recentLabel.isHidden = true
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "edit_icon"), style: .plain, target: self, action: #selector(editProfilePic(_:)))
        self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2
        self.profileImage.clipsToBounds = true
        self.profileImage.layoutIfNeeded()
        picker.delegate = self
        extendedLayoutIncludesOpaqueBars = true
        addSlideMenuButton()
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @IBAction func editPofileAction(_ sender: UIButton) {
        Constant.isProfileEditing = true
        let profile = storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
       // profile.delegates = self
        self.navigationController?.pushViewController(profile, animated: true)
    }
    
  @objc func editProfilePic(_ sender: UIBarButtonItem) {
        if UIImagePickerController.availableMediaTypes(for: .photoLibrary) != nil{
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            present(picker, animated: true, completion: nil)
        }
        else{
            noCamera()
        }
    }
    func noCamera(){
        let alertVC = UIAlertController(title: "No Camera", message: "Sorry, Gallery is not Accessible.", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertVC.addAction(okAction)
        present(alertVC, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        profileImage.contentMode = .scaleToFill
        profileImage.image = chosenImage
        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
//    @IBAction func myAccButton(_ sender: UIButton) {
//        favouriteLabel.isHidden = true
//        recentLabel.isHidden = true
//        accountLabel.isHidden = false
//    }
//
//    @IBAction func FavouriteAction(_ sender: UIButton) {
//        accountLabel.isHidden = true
//        favouriteLabel.isHidden = false
//        recentLabel.isHidden = true
//    }
//
//    @IBAction func recentAction(_ sender: UIButton) {
//        accountLabel.isHidden = true
//        favouriteLabel.isHidden = true
//        recentLabel.isHidden = false
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detailLbl.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellAcc", for: indexPath) as! CellAccountTableViewCell
        // cell = UITableViewCell(style: .subtitle, reuseIdentifier: "CellAcc") as! CellAccountTableViewCell
        cell.textLabel?.text! = detailLbl[indexPath.row]
        cell.detailTextLabel?.text! = self.txt[indexPath.row]
        cell.textLabel?.textColor = Constant.textColor
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == accountDetailTable{
        }
        
    }
    
    func getCode(code: String) {
        getValue = code
    }
    
}
