//
//  Utilities.swift
//  Knektapp
//
//  Created by Gobi R. on 15/11/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import MapKit

class Utilities {
    /*Show alert with message*/
    class func showAlertView(message: String)
    {
        let alert = UIAlertController(title: "Knekt+", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    /*Show alert with message and completion handler*/
    class func showAlertViewWallet(message: String, onView view: UIViewController,enterDoStuff: @escaping (Bool) -> Void)
    {
        let alert = UIAlertController(title: "Knekt+", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action in
            enterDoStuff(true)
        }))
        view.present(alert, animated: true, completion: nil)
    }
    
    /*NavigationBar tint Theme*/
    class func navigationBarTint(control: UIViewController) {
        let navigationBar = control.navigationController?.navigationBar
        navigationBar?.barTintColor = UIColor(red: 24/255, green: 129/255, blue: 88/255, alpha: 1)
        navigationBar?.isTranslucent = false
        navigationBar?.tintColor = UIColor.white
    }
    
    /*hides searchbar*/
    class func hideSearchBar(title: String, control: UIViewController, searchController: UISearchBar){
        searchController.resignFirstResponder()
        searchController.removeFromSuperview()
        Utilities.showTitle(title: title, control: control)
    }
    
    /*initialize SearchBar*/
    class func initSearchBar(control: UIViewController) {
        Constant.searchBar.text = ""
        Constant.searchBar.placeholder = "Search"
        Constant.searchBar.returnKeyType = .done
        Constant.searchBar.becomeFirstResponder()
        Constant.searchBar.delegate = control as? UISearchBarDelegate
        Constant.searchBar.showsCancelButton = true
        control.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        control.navigationItem.titleView = Constant.searchBar
    }
    
    /*Shows the title view*/
    class func showTitle(title: String, control: UIViewController){
        let titleView = UILabel(frame: CGRect(x: 60, y: 0, width: 220, height: 44))
        titleView.text = title
        titleView.font = UIFont.boldSystemFont(ofSize: 22)
        titleView.textColor = .white
        titleView.textAlignment = .center
        control.navigationItem.titleView = titleView
    }
    
    /*Current Location*/
    class func currentLocationFetch(lat: Double, lng: Double, title: String, map: MKMapView) {
        let userLocate = CLLocation(latitude: lat, longitude: lng)
        let myLocation = userLocate as CLLocation
        let pin = MKPointAnnotation()
        pin.title = title
        pin.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        map.setRegion(MKCoordinateRegionMake(myLocation.coordinate, MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)), animated: true)
        map.addAnnotation(pin)
    }
}

/*Label CornerRadius*/
extension UILabel {
    func leftRoundedButton() {
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: [.bottomLeft , .topLeft],
                                     cornerRadii: CGSize(width: 8, height: 8))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
}
