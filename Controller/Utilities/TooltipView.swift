//
//  TooltipView.swift
//  Knektapp
//
//  Created by Gobi R. on 02/08/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit


class TooltipView: UIView {
    
    @IBInspectable var arrowTopLeft: Bool = false
    @IBInspectable var arrowTopCenter: Bool = true
    @IBInspectable var arrowTopRight: Bool = false
    @IBInspectable var arrowBottomLeft: Bool = false
    @IBInspectable var arrowBottomCenter: Bool = false
    @IBInspectable var arrowBottomRight: Bool = false
    
    @IBInspectable var fillColor: UIColor = UIColor(red: 40/255, green: 175/255, blue: 121/255, alpha: 1)
    
//    @IBInspectable var borderColor: UIColor = UIColor(red:0, green:0, blue:0, alpha:0.05)
//    @IBInspectable var borderRadius: CGFloat = 18
//    @IBInspectable var borderWidth: CGFloat = 1
//
//    @IBInspectable var shadowColor: UIColor = UIColor(red:0, green:0, blue:0, alpha:0.14)
//    @IBInspectable var shadowOffsetX: CGFloat = 0
//    @IBInspectable var shadowOffsetY: CGFloat = 2
//    @IBInspectable var shadowBlur: CGFloat = 10
    
    //MARK: - Global Variables
    
    var tooltipWidth = 0
    var tooltipHeight = 0
    
    let trioPath = UIBezierPath()
    
    //MARK: - Initialization
    
    override func draw(_ rect: CGRect) {
        drawTooltip()
    }
    private func topLeft(_ x: CGFloat, _ y: CGFloat) -> CGPoint {
        return CGPoint(x: x, y: y)
    }
    
    private func topRight(_ x: CGFloat, _ y: CGFloat) -> CGPoint {
        return CGPoint(x: CGFloat(tooltipWidth) - x, y: y)
    }
    
    private func bottomLeft(_ x: CGFloat, _ y: CGFloat) -> CGPoint {
        return CGPoint(x: x, y: CGFloat(tooltipHeight) - y)
    }
    
    private func bottomRight(_ x: CGFloat, _ y: CGFloat) -> CGPoint {
        return CGPoint(x: CGFloat(tooltipWidth) - x, y: CGFloat(tooltipHeight) - y)
    }
    
    // Draw methods
    
     func drawTooltip() {
        
        if Constant.loginOrReg == false {
        tooltipWidth = Int(bounds.width/1.8)
        tooltipHeight = Int(bounds.height)
        } else {
            tooltipWidth = Int(bounds.width*1.5)
            tooltipHeight = Int(bounds.height)
        }
        // Define Trio Shape
        
        
        // Top left corner
        
//        bubblePath.move(to: topLeft(0, borderRadius))
//        bubblePath.addCurve(to: topLeft(borderRadius, 0), controlPoint1: topLeft(0, borderRadius / 2), controlPoint2: topLeft(borderRadius / 2, 0))
        
        // Top right corner
        
//        bubblePath.addLine(to: topRight(borderRadius, 0))
//        bubblePath.addCurve(to: topRight(0, borderRadius), controlPoint1: topRight(borderRadius / 2, 0), controlPoint2: topRight(0, borderRadius / 2))
        
        // Bottom right corner
        
//        bubblePath.addLine(to: bottomRight(0, borderRadius))
//        bubblePath.addCurve(to: bottomRight(borderRadius, 0), controlPoint1: bottomRight(0, borderRadius / 2), controlPoint2: bottomRight(borderRadius / 2, 0))
        
        // Bottom left corner
        
//        bubblePath.addLine(to: bottomLeft(borderRadius, 0))
//        bubblePath.addCurve(to: bottomLeft(0, borderRadius), controlPoint1: bottomLeft(borderRadius / 2, 0), controlPoint2: bottomLeft(0, borderRadius / 2))
//        bubblePath.close()
        
        // Arrow
        
        if(arrowTopLeft) {
            trioPath.move(to: topLeft(3, 10))
            trioPath.addLine(to: topLeft(3, -4))
            trioPath.addLine(to: topLeft(16, 2))
            trioPath.close()
        }
        
        if(arrowTopCenter) {
            trioPath.move(to: topLeft(CGFloat((tooltipWidth / 2) - 5), 0))
            trioPath.addLine(to: topLeft(CGFloat(tooltipWidth / 2), -8))
            trioPath.addLine(to: topLeft(CGFloat(tooltipWidth / 2 + 5), 0))
            trioPath.close()
        }
        
        if(arrowTopRight) {
            trioPath.move(to: topRight(16, 2))
            trioPath.addLine(to: topRight(3, -4))
            trioPath.addLine(to: topRight(3, 10))
            trioPath.close()
        }
        
        if(arrowBottomLeft) {
            trioPath.move(to: bottomLeft(16, 2))
            trioPath.addLine(to: bottomLeft(3, -4))
            trioPath.addLine(to: bottomLeft(3, 10))
            trioPath.close()
        } 
        if(arrowBottomCenter) {
            trioPath.move(to: bottomLeft(CGFloat((tooltipWidth / 2) - 5), 0))
            trioPath.addLine(to: bottomLeft(CGFloat(tooltipWidth / 2), -8))
            trioPath.addLine(to: bottomLeft(CGFloat(tooltipWidth / 2 + 5), 0))
            trioPath.close()
        }
        
        if(arrowBottomRight) {
            trioPath.move(to: bottomRight(3, 10))
            trioPath.addLine(to: bottomRight(3, -4))
            trioPath.addLine(to: bottomRight(16, 2))
            trioPath.close()
        }
        
        // Shadow Layer
        
//        let shadowShape = CAShapeLayer()
//        shadowShape.path = bubblePath.cgPath
//        shadowShape.fillColor = fillColor.cgColor
////        shadowShape.shadowColor = shadowColor.cgColor
////        shadowShape.shadowOffset = CGSize(width: CGFloat(shadowOffsetX), height: CGFloat(shadowOffsetY))
////        shadowShape.shadowRadius = CGFloat(shadowBlur)
//        shadowShape.shadowOpacity = 0.8
        
        // Border Layer
        
        let borderShape = CAShapeLayer()
        borderShape.path = trioPath.cgPath
        borderShape.fillColor = fillColor.cgColor
//        borderShape.strokeColor = borderColor.cgColor
//        borderShape.lineWidth = CGFloat(borderWidth*2)
        
        // Fill Layer
        
        let fillShape = CAShapeLayer()
        fillShape.path = trioPath.cgPath
        fillShape.fillColor = fillColor.cgColor
        
        // Add Sublayers
        
     //  self.layer.insertSublayer(shadowShape, at: 0)
        self.layer.addSublayer(borderShape)
        self.layer.addSublayer(fillShape)
        
    }
    
    func removeToolTip() {
        if Constant.loginOrReg == false {
           layer.removeFromSuperlayer()
        } else {
            layer.removeFromSuperlayer()
        }
        
        /* At ViewController
         
         let frame = CGRect(x: 50, y: 50, width: 100, height: 40)
         
         let view = TooltipView(frame: frame)
         view.backgroundColor = UIColor.clear
         view.isOpaque = false
         view.arrowBottomCenter = false // The top center arrow is still visible after this
         view.arrowBottomRight = true // The top right arrow isn't visible after this*/
        
    }
}
