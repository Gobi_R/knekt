//
//  ReusableView.swift
//  Knektapp
//
//  Created by Gobi R. on 10/08/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

public protocol ReusableCell: class {
    static var reuseIdentify: String {get}
}

extension ReusableCell {
    public static var reuseIdentify: String {
        return String(describing: self)
    }
}
extension UITableViewCell: ReusableCell {}
extension UICollectionViewCell: ReusableCell {}

public protocol ReusableCellProvider {
    func register<C: ReusableCell>(cellClass: C.Type)
    func dequeueReusableCell<C: ReusableCell>(ofType cellClass: C.Type, for indexPath: IndexPath) -> C
    func dequeueReusableCell<C: ReusableCell>(with identifier: String, for indexPath: IndexPath) -> C
}
extension UITableView: ReusableCellProvider {
    public func register<C>(cellClass: C.Type) where C : ReusableCell {
        register(cellClass, forCellReuseIdentifier: cellClass.reuseIdentify)
    }
    
    public func dequeueReusableCell<C>(ofType cellClass: C.Type, for indexPath: IndexPath) -> C where C : ReusableCell {
        return dequeueReusableCell(withIdentifier: cellClass.reuseIdentify, for: indexPath) as! C
    }
    
    public func dequeueReusableCell<C>(with identifier: String, for indexPath: IndexPath) -> C where C : ReusableCell {
        return dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! C
    }
    
    
}
extension UICollectionView {
    public func register<C>(cellClass: C.Type) where C : ReusableCell {
        register(cellClass, forCellWithReuseIdentifier: cellClass.reuseIdentify)
    }
    
    public func dequeueReusableCell<C: ReusableCell>(ofType cellClass: C.Type, for indexPath: IndexPath) -> C  {
        return dequeueReusableCell(withReuseIdentifier: cellClass.reuseIdentify, for: indexPath) as! C
    }
        
}
