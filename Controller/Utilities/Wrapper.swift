//
//  Wrapper.swift
//  Knekt+
//
//  Created by Gobi R. on 14/08/18.
//  Copyright © 2018 com.dci. All rights reserved.
//
import UIKit
import Foundation
import Moya
import NVActivityIndicatorView

enum KnektAPI {
    // MARK: - User
    case signup(phone: String, password: String)
    case signin(phone: String, password: String)
//    case forgotPassword(id: Int, password: String)
    case profileUserDataUpdate(type: String, userid: String, name: String, lastname: String, email: String, password: String, phone: String, language: String, dob: String, gender: Int)
//    case profileUserDataGet(type: String, user_id: Int, name: String, lastname: String, email: String, password: String, phone: String, language: String)
    case getFavourites(userId: String)
    case addFavourites(company_id: String, type: String, user_id: String, value: String)
    case deleteFavourites(company_id: String, type: String, user_id: String)
    case getCategory(search_key: String)
    case getSubCategory(category_id: Int, search_key: String, user_id: Int, type: String, subcategory_id: String)
    case getCompanyData(company_id: Int, type: String)
//    case getCompanyKnekt(type: String, company_id: Int)
    case getCountry
}

extension KnektAPI: TargetType {
    
    var baseURL: URL { return URL(string: BaseURL.baseURL)! }
    var path: String {
        switch self {
        case .signup(phone: _, password:_):
            return ApiReqURL.signupUrl
        case .signin(phone: _, password: _):
            return ApiReqURL.signinUrl
//        case .forgotPassword:
//            return ApiReqURL.forgotPasswordUrl
//        case .profileUserDataGet:
//            return ApiReqURL.profileUserData
        case .profileUserDataUpdate(type: _, userid: _, name: _, lastname: _, email: _, password: _, phone: _, language: _, dob: _, gender: _):
            return ApiReqURL.profileUserData
        case .getFavourites(userId: _):
            return ApiReqURL.getFavourites
        case .addFavourites(company_id: _, type: _, user_id: _, value: _):
            return ApiReqURL.addFavourites
        case .deleteFavourites(company_id: _, type: _, user_id: _):
            return ApiReqURL.addFavourites
        case .getCategory(search_key: _):
            return ApiReqURL.getCategorysUrl
        case .getSubCategory(category_id: _, search_key: _, user_id: _, type: _, subcategory_id: _):
            return ApiReqURL.getSubCategoryUrl
        case .getCompanyData:
            return ApiReqURL.getCompanyDataUrl
//        case .getCompanyKnekt:
//            return ApiReqURL.getCompanyKnektUrl
        case .getCountry:
            return ApiReqURL.getCountry
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .signup ,.signin, .getCountry, .profileUserDataUpdate, .getCategory, .getSubCategory, .getCompanyData, .getFavourites, .addFavourites, .deleteFavourites: //,.forgotPassword, .profileUserDataGet, .getCompanyKnekt:
            return .post
        }
    }
    
//    var parameters: [String: Any]? {
//        switch self {
//        case .signup(phone: <#T##String#>, password: <#T##String#>), .signin(let model), .forgotPassword(let model), .profileUserDataGet(let model), .profileUserDataUpdate(let model), .getCategory(let model), .getSubCategory(let model), .getCompanyData(let model), .getCompanyKnekt(let model):
//            return model
//        }
//    }
    
   
var parameterEncoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var sampleData: Data {
        switch self {
        
        case .signup(let phone, let password):
            return "{\"phone\": \(phone), \"password\": \(password)}".utf8Encoded
        case .signin(let phone, let password):
            return "{\"phone\": \(phone), \"password\": \(password)}".utf8Encoded
//        case .forgotPassword(let id, let password):
//            return Data(base64Encoded: "{\"phone\":  \"first_name\": \"Harry\", \"last_name\": \"Potter\"}")!.base64EncodedData()
//        case .profileUserDataGet(let type, let userid):
//            return Data(base64Encoded: "{\"phone\":  \"first_name\": \"Harry\", \"last_name\": \"Potter\"}")!.base64EncodedData()
        case .profileUserDataUpdate(let type, let userid, let name, let lastname, let email, let password, let phone, let language, let dob, let gender):
            return "{\"type\": \(type), \"userid\": \(userid), \"name\": \(name), \"lastname\": \(lastname), \"email\": \(email), \"password\": \(password), \"phone\": \(phone), \"language\": \(language), \"dob\": \(dob), \"gender\": \(gender)}".utf8Encoded
        case .getFavourites(let userId):
            return "{\"user_id\": \(userId)}".utf8Encoded
        case .addFavourites(let company_id, let type, let user_id, let value):
            return "{\"company_id\": \(company_id), \"type\": \(type), \"user_id\": \(user_id), \"value\": \(value)}".utf8Encoded
        case .deleteFavourites(let company_id, let type, let user_id):
            return "{\"company_id\": \(company_id), \"type\": \(type), \"user_id\": \(user_id)}".utf8Encoded
        case .getCategory(let search_key):
            return "{\"search_key\": \(search_key)}".utf8Encoded
        case .getSubCategory(let category_id, let search_key, let user_id, let type, let subcategory_id):
            return "{\"category_id\": \(category_id), \"search_key\": \(search_key), \"user_id\": \(user_id),\"type\": \(type),\"subcategory_id\": \(subcategory_id)}".utf8Encoded
        case .getCompanyData(let company_id, let type):
            return "{\"company_id\": \(company_id), \"type\": \(type)}".utf8Encoded
//        case .getCompanyKnekt(let type, let company_id):
//            return Data(base64Encoded: "{\"phone\": \"first_name\": \"Harry\", \"last_name\": \"Potter\"}")!.base64EncodedData()
        case .getCountry:
            return "".utf8Encoded
        }
      //  return Data()
    }
    
//    var task: Task {
//        switch self {
////        case .nowShowing: // Send no parameters
////            return .requestPlain
//        case let .signup(model), .signin(let model), .forgotPassword(let model), .profileUserDataGet(let model), .profileUserDataUpdate(let model), .getCategory(let model), .getSubCategory(let model), .getCompanyData(let model), .getCompanyKnekt(let model):// Always send parameters as JSON in request body
//            return .requestParameters(parameters:model, encoding: JSONEncoding.default)
//        }
//    }
    
    var task: Task {
        switch self {
//        case .zen, .showUser, .showAccounts: // Send no parameters
//            return .requestPlain
        case let .signup(phone, password):  // Always sends parameters in URL, regardless of which HTTP method is used
            return .requestParameters(parameters: ["phone": phone, "password": password], encoding: URLEncoding.queryString)
        case let .signin(phone, password): // Always send parameters as JSON in request body
            return .requestParameters(parameters: ["phone": phone, "password": password], encoding: URLEncoding.queryString)
//        case .forgotPassword(let id, let password):
//            return .requestParameters(parameters: ["id": id, "password": password], encoding: JSONEncoding.default)
//        case .profileUserDataGet(let type, let userid):
//            return .requestParameters(parameters: ["type": type, "user_id": userid], encoding: JSONEncoding.default)
        case .profileUserDataUpdate(let type, let user_id, let name, let lastname, let email, let password, let phone, let language, let dob, let gender):
            return .requestParameters(parameters: ["type": type, "user_id": user_id, "name": name, "lastname": lastname, "email": email, "password": password, "phone": phone, "language": language, "dob": dob, "gender": gender ], encoding: URLEncoding.queryString)
        case .getFavourites(let userId):
            return .requestParameters(parameters: ["user_id": userId], encoding: URLEncoding.queryString)
        case .addFavourites(let company_id, let type, let user_id, let value):
            return .requestParameters(parameters: ["company_id": company_id, "user_id": user_id, "value": value, "type": type], encoding: URLEncoding.queryString)
        case .deleteFavourites(let company_id, let type, let user_id):
            return .requestParameters(parameters: ["company_id": company_id, "user_id": user_id, "type": type], encoding: URLEncoding.queryString)
        case .getCategory(let search_key):
            return .requestParameters(parameters: ["search_key": search_key], encoding: URLEncoding.queryString)
        case .getSubCategory(let category_id, let search_key, let user_id, let type, let subcategory_id):
            return .requestParameters(parameters: ["category_id": category_id, "search_key": search_key, "user_id": user_id, "type": type, "subcategory_id": subcategory_id], encoding: JSONEncoding.default)
        case .getCompanyData(let company_id, let type):
            return .requestParameters(parameters: ["company_id": company_id, "type": type], encoding: JSONEncoding.default)
//        case .getCompanyKnekt(let type, let company_id):
//            return .requestParameters(parameters: ["type": type, "company_id": company_id], encoding: JSONEncoding.default)
        case .getCountry:
            return .requestPlain
        }
    }
    
    var headers: [String: String]? {
        return ["Content-type": "application/json", "APIAUTHORIZATION": BaseURL.header]
    }
}

private extension String {
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}

struct NetworkAdapter {
  
    static let provider = MoyaProvider<KnektAPI>()
    static func methodPOST(_ strURLName : KnektAPI,  successCallback:@escaping (Response) -> Void,error errorCallback: @escaping (Swift.Error) -> Void, failure failureCallback:@escaping (MoyaError) -> Void) {
        if Reachability.isConnectedToNetwork() {
        provider.request(strURLName) { (result) in
            switch result {
            case .success(let response):
                // 1:
                do {
                    let getJSON = try JSONSerialization.jsonObject(with: response.data, options: []) as! [String: Any]
                    print(getJSON)
                    if response.statusCode >= 200 && response.statusCode <= 299 {
                         NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                        if  getJSON["status"] as? Int == 420 {
                            Utilities.showAlertView(message: (getJSON["msg"] as? String)!)
                            return
                        }else{
                             successCallback(response)
                        }
                    } else if response.statusCode == 420  {
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    } else {
                        // 2:
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                        let error = NSError(domain:"com.vsemenchenko.networkLayer", code:0, userInfo:[NSLocalizedDescriptionKey: "Parsing Error"])
                        errorCallback(error)
                    } 
                    
                } catch {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    print(error)
                }
               
            case .failure(let error):
                // 3:
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                failureCallback(error)
            }
        }
        } else{
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
        Utilities.showAlertView(message: "please Check your Internet Connection")
        }
    }
    
}


