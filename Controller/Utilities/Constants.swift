//
//  Constants.swift
//  Knektapp
//
//  Created by Gobi R. on 02/08/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView

struct Constant {
    static var loginOrReg = false
    static var jsonData = [String: Any]()
    static var defaults = UserDefaults.standard
    static let textColor = UIColor(red: 40/255, green: 175/255, blue: 121/255, alpha: 1.0)
    static var searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
    static var isProfileEditing = false
}

extension UITextField {
    func setPaddingwithCornerRadious(imgName: String){
        self.layer.borderWidth = (1.0 / UIScreen.main.scale) / 4
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.cornerRadius =  16
        let enView = UIImageView(frame: CGRect(x: 5, y: 0, width: 25, height: self.frame.size.height))
        enView.image = UIImage(named: imgName)
        enView.contentMode = .scaleAspectFit
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 35, height: self.frame.size.height))
        paddingView.addSubview(enView)
        paddingView.backgroundColor = .clear
        enView.center = paddingView.center
        
        self.leftView?.frame = enView.frame
        self.leftView = paddingView
        self.leftViewMode = .always
}
    
}
extension CALayer {
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        let border = CALayer()
        switch edge {
        case .top:
            border.frame = CGRect(x: 0, y: 0, width: frame.width, height: thickness)
        case .bottom:
            border.frame = CGRect(x: 0, y: frame.height - thickness, width: frame.width, height: thickness)
        case .left:
            border.frame = CGRect(x: 0, y: 0, width: thickness, height: frame.height)
        case .right:
            border.frame = CGRect(x: frame.width - thickness, y: 0, width: thickness, height: frame.height)
        default:
            break
        }
        border.backgroundColor = color.cgColor;
        addSublayer(border)
    }
}
struct BaseURL {
    // base URL
    static let baseURL = "http://dev.knekt.dci.in/"
    // header
    static let header = "U2VjcmV0OmEyNWxhM1J3YkhWenxQYXNzd29yZDprbmVrdHBsdXMj"
    
    static let otpUrl = "http://control.msg91.com/api/sendotp.php"
    
}

struct ApiReqURL{
    static let signupUrl = "api/Signup"
    static let signinUrl = "api/Signin"
    static let forgotPasswordUrl = "api/Forgotpassword"
    static let profileUserData = "api/Profile"
    static let getFavourites = "api/Getfavourites"
    static let addFavourites = "api/AddFavourite"
    static let getCategorysUrl = "api/GetCategories"
    static let getSubCategoryUrl = "api/GetSubcategories"
    static let getCompanyDataUrl = "api/GetCompanyData"
    static let getCountry = "api/GetCountry"
    /*getCompanyKnektUrl Has 3 params type */
    static let getCompanyKnektUrl = "api/GetCompanyKnekt"
}
struct TAMKeys {
    static let googleKey = "671465856964-b0e17ka29umb77fc53asd3ccbk85nq85.apps.googleusercontent.com"
}

extension UIViewController: NVActivityIndicatorViewable {
    func startAnimate(withMsg: String) {
        startAnimating(NVActivityIndicatorView.DEFAULT_BLOCKER_SIZE, message: withMsg, messageFont: UIFont(name: "Ubuntu", size: 15), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 40/255, green: 175/255, blue: 121/255, alpha: 1.0), padding: NVActivityIndicatorView.DEFAULT_BLOCKER_MESSAGE_SPACING, displayTimeThreshold: NVActivityIndicatorView.DEFAULT_BLOCKER_DISPLAY_TIME_THRESHOLD, minimumDisplayTime: NVActivityIndicatorView.DEFAULT_BLOCKER_MINIMUM_DISPLAY_TIME, backgroundColor: UIColor.black.withAlphaComponent(0.5), textColor: UIColor(red: 40/255, green: 175/255, blue: 121/255, alpha: 1.0), fadeInAnimation: nil)
    }
    func stopAnimate() {
        stopAnimating()
    }
    
}

extension UIViewController {
     func serialization(data: Data) -> Any {
        var jsonData: Any?
        do {
             jsonData = try JSONSerialization.jsonObject(with: data, options: [])
        } catch {
            if let str = String(data: data, encoding: String.Encoding.utf8){
                print("Print Server data:- " + str)
            }
            print("serialization Error")
        }
        return jsonData!
    }
}
extension String   {
    func validateString() -> String {
        return self ?? ""
    }
}

extension String {
    func trim() -> String {
        return self.trimmingCharacters(in: .whitespaces)
    }
    var getDigits: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    }
}
