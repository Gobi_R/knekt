//
//  ViewController.swift
//  Knektapp
//
//  Created by Gowsika on 10/05/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import GoogleSignIn
import Firebase
import CTKFlagPhoneNumber

class ViewController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate, UITextFieldDelegate, Codesets {
    
    var iconsClick: Bool!
    let appdelegate = AppDelegate()
    var web = WebServiceHandler()
    var getdata: [String: Any]!
    var choosenLogin: Bool!
    var retrivedData = [[String: Any]]()
    
    @IBOutlet weak var countryCodeLabel: UILabel!
    @IBOutlet weak var numberTextField: CTKFlagPhoneNumberTextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var forgotPwdOutlet: UIButton!
    @IBOutlet weak var loginOutlet: UIButton!
    @IBOutlet weak var toolTipViewOutlet: TooltipView!
    @IBOutlet weak var loginDownimgOutlet: UIImageView!
    @IBOutlet weak var registerDownImgOutlet: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        numberTextField.parentViewController = self
        numberTextField.setFlag(for: "IND")
        /*Text Field Left View*/
        // numberTextField.setPaddingwithCornerRadious(imgName: "mobile_icon")
        passwordTextfield.setPaddingwithCornerRadious(imgName: "pass_icon")
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tap)
        
        
        //        let navigationBar = navigationController?.navigationBar
        //        navigationBar?.barTintColor = UIColor(red: 24/255, green: 129/255, blue: 88/255, alpha: 1)
        //        navigationBar?.isTranslucent = false
        //        navigationBar?.tintColor = UIColor.white
        Utilities.navigationBarTint(control: self)
        registerDownImgOutlet.isHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        numberTextField.text = ""
        passwordTextfield.text = ""
        countryCodeLabel.text?.removeAll()
        if let arrCode = getdata  {
            self.countryCodeLabel.text! = "\(arrCode["phonecode"] as? Int ?? 0)"
        }
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    //MARK: Class Methods
    @objc func hideKeyboard(){
        view.endEditing(true)
    }
    @objc func getAppstore(){
        let urlStr = "http://search.itunes.apple.com/WebOjects/MZContentLink.woa/wa/link?mt=8&path=appstore"
        if #available(iOS 10.0, *){
            
            UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(URL(string: urlStr)!)
        }
    }
    @objc func nextButtonPressed(sender: UIButton) {
        let selectedProductWebStoreUrl =  "https://www.                                                                                                                                                                                                                                                                                                                                                                                                                                                                 dotcominfoway.com"
        let urlToShare = [selectedProductWebStoreUrl]
        let activityViewController = UIActivityViewController(activityItems: urlToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [UIActivityType.postToWeibo,UIActivityType.assignToContact,
                                                        UIActivityType.print, UIActivityType.saveToCameraRoll, UIActivityType.addToReadingList, UIActivityType.postToFlickr, UIActivityType.postToVimeo, UIActivityType.postToTencentWeibo]
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func numberValidate(value: String) -> Bool {
        let phoneRegex = "[0-9]{10}"
        let numberTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        let result = numberTest.evaluate(with: value)
        return result
    }
    
    func isPasswordValid(_ password: String) -> Bool {
       
        
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[d$@$!%*?&#])[A-Za-z\\dd$@$!%*?&#]{6,12}")
        return passwordTest.evaluate(with: password)
    }
    
    func getWidth(text: String) -> CGFloat {
        let txtField = UITextField(frame: .zero)
        txtField.text = text
        txtField.sizeToFit()
        return txtField.frame.size.width
    }
    
    //MARK: Action Methods
    @IBAction func facebookLogin(_ sender: UIButton) {
        FacebookSdk.FaceBook()
    }
    
    @IBAction func twitterLogin(_ sender: UIButton) {
        TwitterSdk.TwitterLogin()
    }
    
    @IBAction func googleLogin(_ sender: UIButton) {
        GoogleSdk.GooglePlus()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func dropDownAction(_ sender: UIButton) {
        let passVC = storyboard?.instantiateViewController(withIdentifier: "CodeTableViewController") as? CodeTableViewController
        passVC?.delegates = self
        passVC?.retrivedData = self.retrivedData
        self.present(passVC!, animated: true, completion: nil)
    }
    
    @IBAction func showPwdBtn(_ sender: UIButton) {
        if (iconsClick == true){
            passwordTextfield.isSecureTextEntry = false
            iconsClick = false
        }
        else{
            passwordTextfield.isSecureTextEntry = true
            iconsClick = true
        }
    }
   
    //MARK:- Login
    @IBAction func loginButton(_ sender: UIButton) {
        self.view.endEditing(true)
        guard let mobile = numberTextField.text, !mobile.isEmpty else {
            Utilities.showAlertView(message: "mobile field is empty")
            return
        }
        
        guard let password = passwordTextfield.text, !password.isEmpty else {
            Utilities.showAlertView(message: "password field is empty")
            return
        }
        
        if !numberValidate(value: mobile) && !isPasswordValid(password) {
            Utilities.showAlertView(message: "Invalid Mobile or Password")
            return
        } else {
            if Constant.loginOrReg == true {
                passwordTextfield.resignFirstResponder()
                startAnimate(withMsg: "Registering...")
                Constant.defaults.set(numberTextField.getFormattedPhoneNumber(), forKey: "mobileno")
                //                Constant.defaults.set(countryCodeLabel.text, forKey: "code")
                Constant.defaults.set(passwordTextfield.text!, forKey: "password")
                passwordTextfield.resignFirstResponder()
                NetworkAdapter.methodPOST(.signup(phone: numberTextField.getFormattedPhoneNumber()!, password: passwordTextfield.text!), successCallback: { (response) in
                    let resultArray = self.serialization(data: response.data) as! [String: Any]
                    print(resultArray)
                    let data = resultArray["data"] as! [String: Any]
                    Constant.defaults.set(data["id"] as! Int, forKey: "userid")
                    self.stopAnimating()
                    WebServiceHandler.sendOtp(viewController: self, mobile: self.numberTextField.getRawPhoneNumber()!, cc: self.numberTextField.getCountryPhoneCode()!)
                    /*Activating session*/
                    Constant.defaults.set(true, forKey: "Logged")
                    let otpView = self.storyboard?.instantiateViewController(withIdentifier: "SecureViewController") as! SecureViewController
                    otpView.MobileNoStr = self.numberTextField.getRawPhoneNumber()!
                    otpView.cc = self.numberTextField.getCountryPhoneCode()!
                    self.navigationController?.pushViewController(otpView, animated: true)
                }, error: {(Error) in
                    print("error", Error.localizedDescription)
                }, failure: {(Error) in
                    print("error", Error.localizedDescription)
                })
            } else {
                startAnimate(withMsg: "Logging...")
                Constant.defaults.set(numberTextField.getFormattedPhoneNumber(), forKey: "loginMobileNo")
                passwordTextfield.resignFirstResponder()
                NetworkAdapter.methodPOST(.signin(phone: numberTextField.getFormattedPhoneNumber()!, password: passwordTextfield.text!), successCallback: { (response) in
                    let resultArray = self.serialization(data: response.data) as! [String: Any]
                    let data = resultArray["data"] as! [String: Any]
                    self.saveUserDetails(data: data)
                    self.stopAnimate()
                    let nexVc = self.storyboard?.instantiateViewController(withIdentifier: "TabListViewController") as! TabListViewController
                    self.navigationController?.pushViewController(nexVc, animated: true)
                }, error: {(Error) in
                    print("error", Error.localizedDescription)
                }, failure: {(Error) in
                    print("error", Error.localizedDescription)
                })
            }
        }
    }
    
    //MARK:- Forgot password
    @IBAction func forgotPassword(_ sender: UIButton) {
        let forgotPasswordAlert = UIAlertController(title: "Forgot Password!", message: "Enter Registered Mobile Number", preferredStyle: .alert)
        forgotPasswordAlert.addTextField {(textField) in
            textField.placeholder = " Mobile"
            textField.keyboardType = .numberPad
        }
        forgotPasswordAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        forgotPasswordAlert.addAction(UIAlertAction(title: "Reset Password", style: .default, handler: {(action)in
            let resetEmail = forgotPasswordAlert.textFields?.first?.text
            Auth.auth().sendPasswordReset(withEmail: resetEmail!, completion: {(error) in
                if error != nil {
                    let resetFailedAlert = UIAlertController(title: "Reset Failed", message: "Error: \(String(describing: error?.localizedDescription))", preferredStyle: .alert)
                    resetFailedAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(resetFailedAlert, animated: true, completion: nil)
                }
                else {
                    let resetEmailSentAlert = UIAlertController(title: "Reset email sent successfully", message: "Check your email", preferredStyle: .alert)
                    resetEmailSentAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                }
            })
        }))
        self.present(forgotPasswordAlert, animated: true, completion: nil)
    }
    
    @IBAction func loginTitleAction(_ sender: UIButton) {
        Constant.loginOrReg = false
        registerDownImgOutlet.isHidden = true
        loginDownimgOutlet.isHidden = false
        forgotPwdOutlet.isHidden = false
        loginOutlet.setTitle("Login", for: .normal)
    }
    
    @IBAction func registerTitleAction(_ sender: UIButton) {
        Constant.loginOrReg = true
        registerDownImgOutlet.isHidden = false
        loginDownimgOutlet.isHidden = true
        forgotPwdOutlet.isHidden = true
        loginOutlet.setTitle("Register", for: .normal)
    }
    
    //MARK: SDK Delegate Method
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil {
            print(error ?? "google error")
            return
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == numberTextField {
            if let getPlaceHolder = textField.placeholder, !getPlaceHolder.isEmpty {
                return (textField.text?.count ?? 0) <= getPlaceHolder.getDigits.count || string == ""
            }
        }
        return true
    }
    
    func getCode(code: [String: Any]) {
        getdata = code
    }
    
    /*Storing Users information to defaults*/
    func saveUserDetails(data: [String: Any]) {
        Constant.defaults.set(data["id"] as! Int, forKey: "userid")
        Constant.defaults.set(data["photo"] as! String, forKey: "profileImage")
        Constant.defaults.set(data["email"] as! String, forKey: "emailId")
        Constant.defaults.set(data["name"] as! String, forKey: "fName")
        Constant.defaults.set(data["lastname"], forKey: "lName")
        Constant.defaults.set(data["dob"] as! String, forKey: "dob")
        Constant.defaults.set(true, forKey: "Logged")
    }
}

