//
//  ListContainerTableViewController.swift
//  Knektapp
//
//  Created by Gobi R. on 10/08/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class ListContainerTableViewController: UIViewController {
    
    @IBOutlet weak var listTableView: UITableView!
    var listCategories = [categorys]()
    var filtered = [categorys]()
    var isSearchActive = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Utilities.initSearchBar(control: self)
        listTableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        listCategories.removeAll()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        filtered.removeAll()
    }
    
    /*Initial load*/
    func refreshTable() {
        isSearchActive = false
        if !listCategories.isEmpty {
          self.listTableView.reloadData()
        }
    }
    //MARk:- Filtering items in list
    /*Calling this function in TablistViewController to sort searched item*/
    func filterTable(filteredArray: [categorys]) {
        if !filtered.isEmpty {
            filtered.removeAll()
        }
        self.filtered = filteredArray
        isSearchActive = !filtered.isEmpty
        listTableView.reloadData()
    }
    
    /*When canceling the search, reload default */
    func reloadDefault(isCanceld: Bool) {
        filtered.removeAll()
        isSearchActive = isCanceld
        self.listTableView.reloadData()
    }
}
//MARK:- TableView Delegate and datasource
extension ListContainerTableViewController: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSearchActive ? filtered.count : listCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listCell", for: indexPath) as! ListContainerTableCell
        cell.textLabel?.textColor = Constant.textColor
        isSearchActive ? (cell.textLabel?.text = filtered[indexPath.row].categoryTitle ?? "") : (cell.textLabel?.text = listCategories[indexPath.row].categoryTitle ?? "")
        isSearchActive ? (cell.imageView?.sd_setImage(with: URL(string: "\(BaseURL.baseURL)"+"\((filtered[indexPath.row].categoryImages)!)"))) : cell.imageView?.sd_setImage(with: URL(string: "\(BaseURL.baseURL)"+"\((listCategories[indexPath.row].categoryImages)!)"))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let serviceVc = storyboard?.instantiateViewController(withIdentifier: "ServicesViewController") as! ServicesViewController
        serviceVc.categoryId = !filtered.isEmpty ? filtered[indexPath.row].categoryId! : listCategories[indexPath.row].categoryId!
        serviceVc.viewTitle = !filtered.isEmpty ? filtered[indexPath.row].categoryTitle! : listCategories[indexPath.row].categoryTitle!
        self.navigationController?.pushViewController(serviceVc, animated: true)
    }
}
