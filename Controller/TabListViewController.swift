//
//  TabListViewController.swift
//  Knektapp
//
//  Created by Gowsika on 11/05/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import SDWebImage

class TabListViewController: BaseViewController {
    
    @IBOutlet weak var listCollectionView: UICollectionView!
    @IBOutlet weak var categoriesLabel: UILabel!
    @IBOutlet weak var favouriteLabel: UILabel!
    @IBOutlet weak var recentLabel: UILabel!
    @IBOutlet weak var favouriteTableView: UITableView!
    @IBOutlet weak var chooseCategoryLabel: UILabel!
    @IBOutlet weak var tabActionView: UIView!
    @IBOutlet weak var languageLabelOutlet: UILabel!
    @IBOutlet weak var languageSwitchOutlet: UISwitch!
    @IBOutlet weak var makeCategoryListOutlet: UIButton!
    @IBOutlet weak var makeCategroryGridOutlet: UIButton!
    @IBOutlet weak var listTableViewOutlet: UIView!
    @IBOutlet weak var tabbackView: UIView!
    @IBOutlet weak var recentContainerView: UIView!
    
    var searchActive: Bool = false
    var favouritesDetails = [Favourites]()
    var filteredFavourites = [Favourites]()
    var categoryDetails = [categorys]()
    var filtered = [categorys]()
    let slide = BaseViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        favouriteTableView.tableFooterView = UIView()
        Utilities.showTitle(title: "Knekt+", control: self)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        initView()
        /*Gesture Declaration*/
        let edge = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(ScreenEdgeSwiped))
        edge.edges = .right
        view.addGestureRecognizer(edge)
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenSwipeLeft))
        edgePan.edges = .left
        favouriteTableView.addGestureRecognizer(edgePan)
        addSlideMenuButton()
        Utilities.navigationBarTint(control: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.startAnimate(withMsg: "Loading...")
        isListToggled()
        self.categoryDetails.removeAll()
        self.categoryApiCall()
    }
    /*Initializing Search bar to naviagtion bar*/
    @IBAction func searchAction(_ sender: UIBarButtonItem) {
        Utilities.initSearchBar(control: self)
    }
    
    //MARK:- Button Actions
    /*For switching between list or grid*/
    @IBAction func showListAction(_ sender: UIButton) {
        makeCategoryListOutlet.setImage(UIImage(named: "list_fill"), for: .normal)
        makeCategroryGridOutlet.setImage(UIImage(named: "grid_unfill"), for: .normal)
        self.listTableViewOutlet.isHidden = false
        Constant.defaults.set(true, forKey: "listToggled")
    }
    
    @IBAction func showGridAction(_ sender: UIButton) {
        sender.setImage(UIImage(named: "grid"), for: .normal)
        makeCategoryListOutlet.setImage(UIImage(named: "list"), for: .normal)
        self.listTableViewOutlet.isHidden = true
        Constant.defaults.set(false, forKey: "listToggled")
    }
    
    //MARK:- Favour view
    /*Switching between Recent Activities*/
    @IBAction func categoriesAction(_ sender: UIButton) {
        if categoryDetails.isEmpty {
            self.categoryApiCall()
        }
        isListToggled()
        //  (Constant.defaults.value(forKey: "listToggled") != nil)  ? (listTableViewOutlet.isHidden = false) : (listTableViewOutlet.isHidden = true)
        chooseCategoryLabel.isHidden = false
        categoriesLabel.isHidden = false
        favouriteLabel.isHidden = true
        recentLabel.isHidden = true
        listCollectionView.isHidden = false
        favouriteTableView.isHidden = true
        tabbackView.isHidden = false
        recentContainerView.isHidden = true
    }
    
    @IBAction func FavouriteAction(_ sender: UIButton) {
        favouritesDetails.removeAll()
        self.favouritesApiCall()
        favouriteLabel.isHidden = false
        recentLabel.isHidden = true
        categoriesLabel.isHidden = true
        favouriteTableView.isHidden = false
        listCollectionView.isHidden = true
        tabbackView.isHidden = true
        recentContainerView.isHidden = true
        listTableViewOutlet.isHidden = true
    }
    
    @IBAction func recentAction(_ sender: UIButton) {
        recentLabel.isHidden = false
        categoriesLabel.isHidden = true
        favouriteLabel.isHidden = true
        tabbackView.isHidden = true
        listCollectionView.isHidden = true
        favouriteTableView.isHidden = true
        recentContainerView.isHidden = false
        listTableViewOutlet.isHidden = true
    }
    
    //MARK:- Gesture Targets
    /*Switching between Recent Activities*/
    @objc func ScreenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer){
        if recognizer.state == .recognized{
            favouritesDetails.removeAll()
            self.favouritesApiCall()
            listTableViewOutlet.isHidden = true
            favouriteTableView.isHidden = false
            listCollectionView.isHidden = true
            chooseCategoryLabel.isHidden = true
            categoriesLabel.isHidden = true
            favouriteLabel.isHidden = false
            tabbackView.isHidden = true
        }
    }
    
    @objc func screenSwipeLeft(_ recognizer: UIScreenEdgePanGestureRecognizer){
        if recognizer.state == .recognized{
            isListToggled()
            listCollectionView.isHidden = false
            favouriteTableView.isHidden = true
            categoriesLabel.isHidden = false
            favouriteLabel.isHidden = true
            chooseCategoryLabel.isHidden = false
            tabbackView.isHidden = false
        }
    }
    
    /*Checking User chosed List or grid view*/
    func isListToggled() {
        if Constant.defaults.value(forKey: "listToggled") != nil  {
            let isListView = Constant.defaults.value(forKey: "listToggled") as! Bool
            isListView  ? (listTableViewOutlet.isHidden = false) : (listTableViewOutlet.isHidden = true)
            isListView ? makeCategoryListOutlet.setImage(UIImage(named: "list_fill"), for: .normal) : makeCategoryListOutlet.setImage(UIImage(named: "list"), for: .normal)
            isListView ? makeCategroryGridOutlet.setImage(UIImage(named: "grid_unfill"), for: .normal) : makeCategroryGridOutlet.setImage(UIImage(named: "grid"), for: .normal)
        } else {
            listTableViewOutlet.isHidden = true
        }
    }
    
    /*Initialize View*/
    func initView() {
        favouriteLabel.isHidden = true
        recentLabel.isHidden = true
        recentContainerView.isHidden = true
        listCollectionView.layer.cornerRadius = 10
        listCollectionView.layer.masksToBounds = true
        favouriteTableView.isHidden = true
        chooseCategoryLabel.isHidden = false
    }
}

//MARK:- TableView Datasource Methods
extension TabListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favouritesDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.favouriteTableView.dequeueReusableCell(withIdentifier: "FavourCell", for: indexPath) as! FavoriteTableViewCell
        cell.textLabel?.text = self.favouritesDetails[indexPath.row].serviceName
        cell.imageView?.sd_setImage(with: URL(string: "\(BaseURL.baseURL)"+"\(self.favouritesDetails[indexPath.row].serviceImage ?? "")"))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView  == favouriteTableView {
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete =  UIContextualAction(style: .normal, title: nil, handler: { (action,view,completionHandler) in
            NetworkAdapter.methodPOST(.addFavourites(company_id: self.favouritesDetails[indexPath.row].favouritesId!, type: "remove", user_id: "\(Constant.defaults.value(forKey: "userid") as? Int ?? 0)", value: self.favouritesDetails[indexPath.row].favouritesId!), successCallback: { (response) in
                if let jsonData = self.serialization(data: response.data) as? [String: Any] {
                    print(jsonData)
                    action.backgroundColor = UIColor.red
                    self.favouritesDetails.remove(at: indexPath.row)
                    tableView.deleteRows(at: [indexPath], with: .left)
                }
                //            tableView.reloadData()
            }, error: { (error) in
                print("removeFav response error: \(error)")
            }, failure: { (moyaError) in
                print("removeFav moya error: \(moyaError)")
            })
            
        })
        delete.image = UIImage(named: "delete")
        let confrigation = UISwipeActionsConfiguration(actions: [delete])
        return confrigation
    }
}

//MARK:- Collectionview
extension TabListViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    //MARK:- CollectionView datasource Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return searchActive ? filtered.count : categoryDetails.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CellListCollectionViewCell
        cell.layer.borderWidth = 0.7
        cell.layer.borderColor = UIColor.lightGray.cgColor
        searchActive ? (cell.ItemImageView.sd_setImage(with: URL(string: "\(BaseURL.baseURL)"+"/\((filtered[indexPath.row].categoryImages)!)"))) : (cell.ItemImageView.sd_setImage(with: URL(string: "\(BaseURL.baseURL)"+"/\((categoryDetails[indexPath.row].categoryImages)!)")))
        searchActive ? (cell.itemLabel.text =  self.filtered[indexPath.row].categoryTitle) : (cell.itemLabel.text =  categoryDetails[indexPath.row].categoryTitle)
        return cell
    }
    
    //MARK:- CollectionView Delegate Method
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) { 
        let serviceVc = storyboard?.instantiateViewController(withIdentifier: "ServicesViewController") as! ServicesViewController
        serviceVc.categoryId = !filtered.isEmpty ? filtered[indexPath.row].categoryId! : categoryDetails[indexPath.row].categoryId!
        serviceVc.viewTitle = !filtered.isEmpty ? filtered[indexPath.row].categoryTitle! : categoryDetails[indexPath.row].categoryTitle!
        self.navigationController?.pushViewController(serviceVc, animated: true)
    }
    
    /*CollectionView Flowlayout*/
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/3, height: collectionView.frame.height/4)
    }
    
}

//MARK:- Searchbar Delegate Methods
extension TabListViewController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        filtered.removeAll()
        searchBar.text = ""
        let listView = self.childViewControllers[1] as! ListContainerTableViewController
        listView.reloadDefault(isCanceld: false)
        Utilities.hideSearchBar(title: "Knekt+", control: self, searchController: searchBar)
        listCollectionView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let searchString = searchBar.text,!searchString.isEmpty else {
            filtered.removeAll()
            listCollectionView.reloadData()
            return
        }
        categoryApiCallWithSearchKey(SearchedKey: searchString)
        //        filtered = categoryDetails.filter { (item) -> Bool in
        //            // If dataItem matches the searchText, return true to include it
        //            return item.categoryTitle!.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        //        }.sorted{($0.categoryTitle!.hasPrefix(searchText) ? 0 : 1) < ($1.categoryTitle!.hasPrefix(searchText) ? 0 : 1)}
        listCollectionView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
        let listView = self.childViewControllers[1] as! ListContainerTableViewController
        listView.reloadDefault(isCanceld: searchActive)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBarWhenClosed(searchBar: searchBar)
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBarWhenClosed(searchBar: searchBar)
    }
    
    func searchBarWhenClosed(searchBar: UISearchBar) {
        searchActive = false
        let listView = self.childViewControllers[1] as! ListContainerTableViewController
        listView.reloadDefault(isCanceld: false)
        Utilities.hideSearchBar(title: "Knekt+", control: self, searchController: searchBar)
        listCollectionView.reloadData()
    }
    
    //MARK:- Category Api Call
    func categoryApiCall() {
        NetworkAdapter.methodPOST(.getCategory(search_key: ""), successCallback: { (response) in
            let jsonData = self.serialization(data: response.data) as! [String: Any]
            if let dat = jsonData["data"] as? [String: Any], !dat.isEmpty, let data = dat["data"] as? [[String: Any]], !data.isEmpty{
                for values in data {
                    self.categoryDetails.append(categorys(categoryImages: values["category_image"] as? String ?? "", categoryTitle: values["category_name"] as? String ?? "", categoryId: values["id"] as! Int))
                }
                /*Archiving view hirierchy to get tableview container
                 send data to ListContainerTableViewController and reloading tableview*/
                DispatchQueue.main.async {
                    let listView = self.childViewControllers[1] as! ListContainerTableViewController
                    listView.listCategories.append(contentsOf: self.categoryDetails)
                    listView.refreshTable()
                    self.listCollectionView.reloadData()
                }
            } else {
                Utilities.showAlertView(message: "No such data found")
            }
        }, error: { (error) in
            print("response error: \(error)")
        }) { (moyaError) in
            print("moyaError\(moyaError)")
        }
    }
    
    func favouritesApiCall() {
        self.startAnimate(withMsg: "Loading...")
        //\(Constant.defaults.value(forKey: "userid") as! Int)
        NetworkAdapter.methodPOST(.getFavourites(userId: "\(Constant.defaults.value(forKey: "userid") as! Int)" ), successCallback: { (response) in
            if let jsonData = self.serialization(data: response.data) as? [String: Any],!jsonData.isEmpty,  let data = jsonData["data"] as? [[String: Any]], !data.isEmpty{
                let companyData = data.compactMap({$0 ["company_data"] as? [String: Any]})
                if !companyData.isEmpty{
                    for values in companyData {
                        self.favouritesDetails.append(Favourites(serviceName: values["service_name"] as? String ?? "", serviceImage: values["service_image"] as? String ?? "", favouritesId: values["id"] as? String ?? ""))
                    }
                    self.stopAnimate()
                }
                DispatchQueue.main.async {
                    if !self.favouritesDetails.isEmpty{
                        self.favouriteTableView.reloadData()
                    }
                }
            } else {
                Utilities.showAlertView(message: "No such data found")
            }
        }, error: { (error) in
            print("favourites response error: \(error)")
        }) { (moyaError) in
            print("favourites moyaError\(moyaError)")
        }
    }
    
    /*CategoryApiCallWith Searched Key*/
    func categoryApiCallWithSearchKey(SearchedKey: String) {
        filtered.removeAll()
        self.startAnimate(withMsg: "Searching")
        NetworkAdapter.methodPOST(.getCategory(search_key: SearchedKey), successCallback: { (response) in
            let jsonData = self.serialization(data: response.data) as! [String: Any]
            if let dat = jsonData["data"] as? [String: Any], !dat.isEmpty, let data = dat["data"] as? [[String: Any]], !data.isEmpty {
                for values in data {
                    self.filtered.append(categorys(categoryImages: values["category_image"] as? String ?? "", categoryTitle: values["category_name"] as? String ?? "", categoryId: values["id"] as! Int))
                }
                DispatchQueue.main.async {
                    let listView = self.childViewControllers[1] as! ListContainerTableViewController
                    self.filtered = self.filtered.sorted{($0.categoryTitle!.hasPrefix(SearchedKey) ? 0 : 1) < ($1.categoryTitle!.hasPrefix(SearchedKey) ? 0 : 1)}
                    listView.filterTable(filteredArray: self.filtered)
                    self.listCollectionView.reloadData()
                }
            } else {
                Utilities.showAlertView(message: "No such data found")
            }
        }, error: { (error) in
            print("response error: \(error)")
        }) { (moyaError) in
            print("moyaError\(moyaError)")
        }
    }
}

