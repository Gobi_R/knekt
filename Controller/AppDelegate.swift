//
//  AppDelegate.swift
//  Knektapp
//
//  Created by Gowsika on 10/05/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import GoogleSignIn
import TwitterKit
import FBSDKLoginKit
import FacebookLogin
import Firebase
import GoogleMaps


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {
    
    var locationManager: CLLocationManager!
    var window: UIWindow?
    var navigation: UINavigationController?
    var MobileNoStr = String()
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            // Perform any operations on signed in user here.
            _ = user.userID                  // For client-side use only!
            _ = user.authentication.idToken // Safe to send to the server
            _ = user.profile.name
            _ = user.profile.givenName
            _ = user.profile.familyName
            _ = user.profile.email
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        GIDSignIn.sharedInstance().clientID = "613156707176-m9bilt0t0bmi5app8iiej22qf9m9bgqj.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        TWTRTwitter.sharedInstance().start(withConsumerKey: "nHW5D7Ex7dCFCut1UgPGZ93sU", consumerSecret: "8vPwvl7X3vKo6WcfD9XiN6nzezzw4yJFLwekGL7QFGb7pJzI0V")
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        GMSServices.provideAPIKey("AIzaSyC_w5IuWVINn4zmpvvVfcV06iO6iG5Gv6w")
        FirebaseApp.configure()
        /*Session Active Checking*/
        LoginStatus.updateStatus()
        return true
    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        let googleDidHandle = GIDSignIn.sharedInstance().handle(url,sourceApplication: sourceApplication,
                                                                annotation: annotation)
        let facebookDidHandle = FBSDKApplicationDelegate.sharedInstance().application(application,open: url,sourceApplication: sourceApplication,annotation: annotation)
        return googleDidHandle && facebookDidHandle
    }
    func application(_ app: UIApplication, open url: URL, options option: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool{
        return TWTRTwitter.sharedInstance().application(app, open: url, options: option)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
}
