//
//  RecentViewController.swift
//  Knektapp
//
//  Created by Gobi R. on 16/08/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class RecentViewController: UIViewController,UITableViewDataSource {

    @IBOutlet weak var recentTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recentTableView.tableFooterView = UIView()
    }

    //MARK:- Tableview datasource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "recentCell", for: indexPath)
    //    cell.textLabel?.text = recentData[indexPath.row]
        return cell
    }
}
