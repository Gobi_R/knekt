//
//  WebsiteViewController.swift
//  Knektapp
//
//  Created by Gowsika on 25/05/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class WebsiteViewController: UIViewController, UIWebViewDelegate {
    var urls = String()
@IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
     webView.delegate = self
        let url = URL (string: urls)
        let requestObj = URLRequest(url: url!)
        webView.loadRequest(requestObj)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      //  self.navigationController?.setNavigationBarHidden(true, animated: true)
    }


}
