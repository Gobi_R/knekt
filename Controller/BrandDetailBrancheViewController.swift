//
//  BrandDetailBrancheViewController.swift
//  Knektapp
//
//  Created by Gowsika on 30/05/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class BrandDetailBrancheViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    var companyId = 0
    var branchDetails = [BranchDetails]()
    var companyData = [[String: Any]]()
    var cellExpanded = false
    
    @IBOutlet weak var bankTitleImg: UIImageView!
    @IBOutlet weak var branchTabel: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startAnimate(withMsg: "Loading...")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "favourites"), style: .plain, target: self, action: #selector(favouriteAction))
        self.branchTabel.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NetworkAdapter.methodPOST(.getCompanyData(company_id: companyId, type: "branch"), successCallback: { (response) in
            if let jsonData = self.serialization(data: response.data) as? [String: Any], !jsonData.isEmpty {
                print(jsonData)
                if let data = jsonData["data"] as? [String: Any] {
                    let deepData = data["data"] as! [[String: Any]]
                    for data in deepData {
                        self.companyData.append(data["company_data"] as! [String: Any])
                    }
                    for details in self.companyData {
                        self.branchDetails.append(BranchDetails(branchName: details["service_name"] as? String ?? "", branchAddress: details["address"] as? String ?? "", lat: details["latitude"] as? String ?? "", lng: details["langitude"] as? String ?? "", isExpanded: false))
                        self.bankTitleImg.sd_setImage(with: URL(string: "\(BaseURL.baseURL)"+"/\(details["service_image"] as? String ?? "")"))
                    }
                    self.branchTabel.reloadData()
                }
                self.stopAnimate()
            }
        }, error: { (error) in
            print("branch Error: \(error)")
        }) { (moyaError) in
            print("branch MoyaError: \(moyaError)")
        }
    }
    
    @objc func favouriteAction() {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return branchDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell4", for: indexPath) as! BranchDetailTableViewCell
        guard !branchDetails.isEmpty else { return cell }
        cell.serviceName.text! = branchDetails[indexPath.row].branchName!
        cell.serviceAddress.text! = branchDetails[indexPath.row].branchAddress!
        cell.serviceAddress.layoutIfNeeded()
        Utilities.currentLocationFetch(lat: Double("\((branchDetails[indexPath.row].lat)!)")!, lng: Double("\((branchDetails[indexPath.row].lng)!)")!, title: branchDetails[indexPath.row].branchName!, map: cell.branchMap)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return branchDetails[indexPath.row].isExpanded ? 250 : 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.branchDetails[indexPath.row].isExpanded = !self.branchDetails[indexPath.row].isExpanded
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    /*Assigning lat,lng*/
    
}
