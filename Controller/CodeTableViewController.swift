//
//  CodeTableViewController.swift
//  Knektapp
//
//  Created by Gowsika on 10/05/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import SDWebImage

class CodeTableViewController: UIViewController, UITableViewDelegate,UISearchBarDelegate,UITableViewDataSource {
    
    var arrayOfCountryName = [String]()
    var arrayofCountryCode = [String]()
    var filterdarrayOfCountryName = [String]()
    var retrivedData = [[String: Any]]()
    var searchActive = Bool()
    var delegates: Codesets?
    var strfun: getCountrycode?
    @IBOutlet weak var codeSearchBar: UISearchBar!
    @IBOutlet weak var codeTableViewOutlet: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        codeSearchBar.delegate = self
        codeSearchBar.showsCancelButton = true
        codeSearchBar.placeholder = "Search"
        let rightEditBarButtonItem: UIBarButtonItem = UIBarButtonItem(title: "Close", style: UIBarButtonItemStyle.plain, target: self, action: #selector(closeAction))
        self.navigationItem.setRightBarButtonItems([rightEditBarButtonItem], animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    @objc func closeAction(){
        codeTableViewOutlet.isHidden = true
    }
    //MARK: TableView Datasource Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (searchActive){
            return filterdarrayOfCountryName.count
        }
        return retrivedData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! CellCodeTableViewCell
        if (searchActive) {
            cell.countryNameLabel.text = filterdarrayOfCountryName[indexPath.row]
            //cell.countryCodeLabel.text = arrayofCountryCode[indexPath.row]
        }
        else {
            cell.countryNameLabel.text! = retrivedData[indexPath.row]["name"] as! String
            cell.countryCodeLabel.text! = "\(retrivedData[indexPath.row]["phonecode"] as! Int)"
            cell.imageView?.sd_setImage(with: URL(string: "\(BaseURL.baseURL)"+"uploads/\(self.retrivedData[indexPath.row]["flag"] as! String )"))
        }
        return cell
    }
    //MARK:- TableView Delegate Methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegates!.getCode(code: retrivedData[indexPath.row])
        let transition: CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionReveal
        transition.subtype = kCATransitionFromBottom
        self.view.window!.layer.add(transition, forKey: nil)
        self.dismiss(animated: false, completion: nil)
        
    }
    //MARK: SearchBar Delegate Methods
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
        codeSearchBar.showsCancelButton = true
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        codeSearchBar.text = nil
        codeSearchBar.showsCancelButton = false
        searchActive = false
        codeSearchBar.endEditing(true)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = true
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let predicate = NSPredicate(format: "SELF CONTAINS[c] %@" ,searchText)
        var Names = [String]()
            //arrayOfCountryName.filter { predicate.evaluate(with: $0) }
       let countryNames = retrivedData.compactMap{$0["name"]}
        Names = countryNames.filter {predicate.evaluate(with: $0)} as! [String]
        print(Names)
        if Names.count == 0 {
            filterdarrayOfCountryName = retrivedData.compactMap{$0["name"]} as! [String]
        }
        else{
            filterdarrayOfCountryName = Names
        }
        codeTableViewOutlet.reloadData()
    }
    
}
protocol Codesets {
    func getCode(code: [String: Any])
    
    
}
protocol getCountrycode {
    func arrCode(set: String)
}
