//
//  ServicesCollectionViewCell.swift
//  Knektapp
//
//  Created by Gowsika on 28/05/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class ServicesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var logoImages: UIImageView!
}
