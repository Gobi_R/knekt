//
//  DatePickerViewController.swift
//  Knektapp
//
//  Created by Gobi R. on 19/11/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class DatePickerViewController: UIViewController {

    var delegate: sendDate?
    
    @IBOutlet weak var datePickerOutlet: UIDatePicker!
    override func viewDidLoad() {
        super.viewDidLoad()
        let date = Date()
        datePickerOutlet.maximumDate = date
    }
    
    @IBAction func datePickerValueChanged(_ sender: UIDatePicker) {
        let components = Calendar.current.dateComponents([.month, .day, .year], from: sender.date)
        if let day = components.day, let month = components.month, let year = components.year {
            print(day,month,year)
         //   dob = "\(year)-\(month)-\(day)"
            let wmonth = DateFormatter().monthSymbols[month - 1]
            guard delegate != nil else {return}
            delegate?.date(day: day, month: wmonth, year: year)
        //    dateLabel.text! = "\(day)"
        //    monthLabelOutlet.text! = "\(wmonth)"
        //    yearLabel.text! = "\(year)"
        }
    }
    @IBAction func doneAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

protocol sendDate {
    func date(day: Int, month: String, year: Int)
}
