//
//  BankServiceTableViewController.swift
//  Knektapp
//
//  Created by Gowsika on 25/05/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import MessageUI
class BankServiceTableViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, MFMailComposeViewControllerDelegate {
  var service = ["Email", "SMS", "WhatsApp", "Website", "Live Chat", "Call"]
    var urlWeb = [String: Any]()
    
    let defaults = UserDefaults.standard
    var myDict = Dictionary<String, Int>()
    let myKey = "Key"
    var website = ["https://www.axisbank.com", "https://www.hdfcbank.com", "http://www.almasraf.ae","https://rakbank.ae/wps/portal/retail-banking","https://dubaiholding.com/en/media-hub/press-releases/dubai-holding-launch-regions-first-digital-bank","https://www.fgbgroup.com/personal-and-business-banking","https://www.bankfortheuae.com/en/home/","https://nbf.ae/en","https://www.mashreqbank.com/uae/en/personal/home","https://www.hsbc.co.in/1/2/homepage","http://www.dib.ae","https://www.qnb.com/cs/Satellite/QNBQatar/en_QA/enHome","https://www.meezanbank.com","https://www.cnbbank.bank","https://www.fidorbank.uk","https://www.online.citibank.co.in"]
   var images = String()
    @IBOutlet weak var BankServiceTable: UITableView!
    @IBOutlet weak var bankLogoImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
   defaults.set(website, forKey: images)
    self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    BankServiceTable.delegate = self
      BankServiceTable.dataSource = self
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.bankLogoImage.image = UIImage(named: images)
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "menu (1)"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(nextButtonPressed), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        let btn2 = UIButton(type: .custom)
        btn2.setImage(UIImage(named: "favorite (1)"), for: .normal)
        btn2.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn2.addTarget(self, action: #selector(nextButtonPressed), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        self.navigationItem.setRightBarButtonItems([item1,item2], animated: true)
    }
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
     
    }
    @objc func nextButtonPressed(){
        
    }
    func getWhatsApp(){
        let urlString = "Sending WhatsApp message through app in Swift"
let urlStringEncoded = urlString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
        let url  = NSURL(string: "whatsapp://send?text=\(urlStringEncoded!)")
        
        if UIApplication.shared.canOpenURL(url! as URL) {
            UIApplication.shared.openURL(url! as URL)
        } else {
            let errorAlert = UIAlertView(title: "Cannot Send Message", message: "Your device is not able to send WhatsApp messages.", delegate: self, cancelButtonTitle: "OK")
            errorAlert.show()
        }
    }
    func getEmail(){
       let compose = MFMailComposeViewController()
        compose.mailComposeDelegate = self
        compose.setToRecipients(["smgowshi95@gmail"])
        self.present(compose, animated: true, completion: nil)
    }
    func getSMS(){
        guard let msgAppURL = NSURL(string: "sms:")
            else{
        return
            }
        if UIApplication.shared.canOpenURL(msgAppURL as URL){
            UIApplication.shared.openURL(msgAppURL as URL)
        }

    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return service.count
    }
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellBankServiceTableViewCell = self.BankServiceTable.dequeueReusableCell(withIdentifier: "Cell1") as! CellBankServiceTableViewCell
        cell.serviceLabel.text = self.service[indexPath.row]
     cell.accessoryType = .disclosureIndicator
        if indexPath.row == 5 {
            var button = UIButton()
            let images = UIImage(named: "phone-receiver")
            button = UIButton(frame: CGRect(x: BankServiceTable.frame.width - 30, y: 12, width: 20, height: 20))
           button.setImage(images, for: .normal)
    //button.imageEdgeInsets = UIEdgeInsets(top: 5, left: cell.frame.size.width, bottom: 5, right: cell.frame.size.height)
            cell.addSubview(button)
            cell.accessoryType = .none
        }
    return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selected = self.service[indexPath.row]
       if (selected .isEqual("WhatsApp")){
            getWhatsApp()
            }else if (selected .isEqual("Email")){
            getEmail()
    }
        else if (selected .isEqual("SMS")){
            getSMS()
        }
        else if(selected .isEqual("Website")){
        let str = storyboard?.instantiateViewController(withIdentifier: "WebsiteViewController") as! WebsiteViewController
         str.urls = "https://www.axisbank.com"
          self.navigationController?.pushViewController(str, animated: true)
        }
        else if (selected .isEqual("Call")){
           let go = storyboard?.instantiateViewController(withIdentifier: "LanguageViewController") as! LanguageViewController
        
            go.logos.append(images)
            self.navigationController?.pushViewController(go, animated: true)
        }
        else if (selected .isEqual("Live Chat")){
        let setting = storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        self.navigationController?.pushViewController(setting, animated: true)
        }
}
}
