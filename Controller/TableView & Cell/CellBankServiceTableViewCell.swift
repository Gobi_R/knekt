//
//  CellBankServiceTableViewCell.swift
//  Knektapp
//
//  Created by Gowsika on 25/05/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
class CellBankServiceTableViewCell: UITableViewCell {
    
    var iconClick: Bool!
    var isFav = UserDefaults.standard.bool(forKey: "isFav")
    let tableView = BankServiceTableViewController()
    
  @IBOutlet weak var serviceLabel: UILabel!
  @IBOutlet weak var favBtnOutlet: UIButton!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
      }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
}
@IBAction func favButton(_ sender: UIButton) {
        if isFav {
            let images = UIImage(named: "favourite-heart-2")
            favBtnOutlet.setImage(images, for: .normal)
    }
        else{
            let img = UIImage(named: "favorite-2")
            favBtnOutlet.setImage(img, for: .normal)
        }
        isFav = !isFav
        UserDefaults.standard.set(isFav, forKey: "isFav")
        UserDefaults.standard.synchronize()
    let favorite = UITableViewRowAction(style: .normal, title: "Favorite") { (action, indexPath) in
        var favorites : [String] = []
       
    if let favoritesDefaults : AnyObject = UserDefaults.standard.object(forKey: "favorites") as AnyObject {
            favorites = favoritesDefaults as! [String]
        }
 //   favorites.append(tableView.BankServiceTable.cellForRow(at: indexPath))
     UserDefaults.standard.set(favorites, forKey: "favorites")
        UserDefaults.standard.synchronize()
    }
    }
}
