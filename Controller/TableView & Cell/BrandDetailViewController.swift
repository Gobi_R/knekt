//
//  BrandDetailViewController.swift
//  Knektapp
//
//  Created by Gowsika on 30/05/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class BrandDetailViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
    var brand = ["Debit Card PIN,Credit card PIN & telebanking PIN releted services", "Savings account", "credit card releted query", "credit Loan related query", "Line & UPI related query", "Application stats or apply new product "]
    var far = ["favourite-heart-2", "favourite-heart-2","favourite-heart-2", "favourite-heart-2","favourite-heart-2","favourite-heart-2"]
    var emo = String()
   
@IBOutlet weak var logoBank: UIImageView!
@IBOutlet weak var brandTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        brandTable.delegate = self
        brandTable.dataSource = self
       self.logoBank.image = UIImage(named: emo)
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "menu (1)"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(nextButtonPressed), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        let btn2 = UIButton(type: .custom)
        btn2.setImage(UIImage(named: "favorite (1)"), for: .normal)
        btn2.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn2.addTarget(self, action: #selector(nextButtonPressed), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        self.navigationItem.setRightBarButtonItems([item1,item2], animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    @objc func nextButtonPressed(){
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return brand.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: BrandTableViewCell = self.brandTable.dequeueReusableCell(withIdentifier: "Cell3", for: indexPath) as!  BrandTableViewCell
        cell.serviceLabel.text = self.brand[indexPath.row]
        cell.serviceLabel.numberOfLines = 2
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selected = brand[indexPath.row]
        if (selected .isEqual(brand[indexPath.row])){
                let next = storyboard?.instantiateViewController(withIdentifier: "AccountViewController") as! AccountViewController
            self.navigationController?.pushViewController(next, animated: true)
            }
        }
    }


