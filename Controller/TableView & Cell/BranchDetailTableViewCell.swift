//
//  BranchDetailTableViewCell.swift
//  Knektapp
//
//  Created by Gowsika on 30/05/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import MapKit

class BranchDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var branchMap: MKMapView!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var serviceAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
