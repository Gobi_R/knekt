//
//  LanguageViewController.swift
//  Knektapp
//
//  Created by Gowsika on 30/05/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class LanguageViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   var language = ["Arabic", "English"]
var logos = String()
    
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var languageTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
       languageTable.delegate = self
        languageTable.dataSource = self
        self.logoImage.image = UIImage(named: logos)
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "menu (1)"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(nextButtonPressed), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        let btn2 = UIButton(type: .custom)
        btn2.setImage(UIImage(named: "favorite (1)"), for: .normal)
        btn2.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn2.addTarget(self, action: #selector(nextButtonPressed), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        self.navigationItem.setRightBarButtonItems([item1,item2], animated: true)
    }
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
      
    }
    @objc func nextButtonPressed(){
        
    }
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return language.count
    }
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: LanguageTableViewCell = self.languageTable.dequeueReusableCell(withIdentifier: "Cell2", for: indexPath)  as! LanguageTableViewCell
        cell.languageLabel.text = self.language[indexPath.row]
       cell.accessoryType = .disclosureIndicator
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selected = language[indexPath.row]
        if (selected .isEqual(language[indexPath.row])){
            let pass = storyboard?.instantiateViewController(withIdentifier: "BrandDetailViewController") as! BrandDetailViewController
            pass.emo.append(logos)
            self.navigationController?.pushViewController(pass, animated: true)
        }
    }
    

}
