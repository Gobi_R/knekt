//
//  BrandTableViewCell.swift
//  Knektapp
//
//  Created by Gowsika on 30/05/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class BrandTableViewCell: UITableViewCell {
    var iconClick: Bool!
    var isFav = UserDefaults.standard.bool(forKey: "isFav")
    @IBOutlet weak var serviceLabel: UILabel!
    
    @IBOutlet weak var favouriteOutlet: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
       }
override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
}
@IBAction func favouriteAction(_ sender: UIButton) {
        if isFav {
            let images = UIImage(named: "favourite-heart-2")
            favouriteOutlet.setImage(images, for: .normal)
        }
        else{
            let img = UIImage(named: "favorite-2")
            favouriteOutlet.setImage(img, for: .normal)
        }
        isFav = !isFav
        UserDefaults.standard.set(isFav, forKey: "isFav")
        UserDefaults.standard.synchronize()
    }
}
