//
//  SettingsViewController.swift
//  Knektapp
//
//  Created by Gowsika on 30/05/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import UserNotifications
class SettingsViewController: UIViewController {
@IBOutlet weak var switchNotify: UISwitch!
    override func viewDidLoad() {
        super.viewDidLoad()
        extendedLayoutIncludesOpaqueBars = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
      
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    @IBAction func SwitchAction(_ sender: UISwitch) {
        if switchNotify.isOn
        {
            if #available(iOS 10.0, *){
                let content = UNMutableNotificationContent()
                content.title = "You have tasks to complete!"
                content.subtitle = ""
                content.body = "Open the task manager to see which tasks need completion"
                let alarmTime = Date().addingTimeInterval(60)
                let components = Calendar.current.dateComponents([.weekday,
                                                                  .hour, .minute], from: alarmTime)
                let trigger = UNCalendarNotificationTrigger(dateMatching:
                    components, repeats: true)
                let request = UNNotificationRequest(identifier:
                    "taskreminder", content: content, trigger: trigger)
                UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
            } else {
                print ("hello")
            }
            
        } else{
           UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        }
        
       
    }
    

}
